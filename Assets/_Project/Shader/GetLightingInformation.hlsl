﻿void GetLightingInformation_float(float3 ObjPos, out float3 Direction, out float3 Color,out float ShadowAttenuation)
{
#ifdef SHADERGRAPH_PREVIEW
    //Hardcoded data, used for the preview shader inside the graph
    Direction = float3(-0.5,0.5,-0.5);
    Color = float3(1,1,1);
    ShadowAttenuation = 1;
#else
    //Actual light data from the pipeline
    Light light = GetMainLight(GetShadowCoord(GetVertexPositionInputs(ObjPos)));
    Direction = light.direction;
    Color = light.color;
    ShadowAttenuation = light.shadowAttenuation;
#endif
}