﻿using System;

namespace Kniblings.StateMachine
{
    [Serializable]
    public class Transition
    {
        public Decision decision;
        public State falseState;
        public State trueState;
    }
}