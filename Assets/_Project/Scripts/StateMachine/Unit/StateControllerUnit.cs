﻿using System.Collections.Generic;
using Kniblings.Combat;
using Kniblings.Data;
using Kniblings.Locomotion;
using Kniblings.Other;
using Kniblings.Selection;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.StateMachine.Unit
{
    public class StateControllerUnit : StateController
    {
        #region Variables

        [BoxGroup("Unit")] [SerializeField] private UnitData unitData;

        [BoxGroup("Unit")] [SerializeField] private bool isAggressive;

        [BoxGroup("States")] public Damageable absoluteTarget;

        [BoxGroup("States")] public Damageable currentTarget;

        [BoxGroup("States")] public Vector3 moveDestination;

        [BoxGroup("States")] [SerializeField] private State idleAggressiveState;

        [BoxGroup("States")] [SerializeField] private State idlePassiveState;

        [BoxGroup("States")] [SerializeField] private State deadState;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private AttackBase attackBase;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private Damageable damageable;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private UnitMovement unitMovement;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private Selectable selectable;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private Animator animator;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private Collider damageCollider;

        [FoldoutGroup("Components")] [Required] [SerializeField]
        private AudioSource audioSource;

        #endregion

        #region Properties

        public List<Vector3> PatrolWayPoints { get; set; }
        public int NextWayPoint { get; set; }
        public UnitData UnitData => unitData;
        public AttackBase AttackBase => attackBase;
        public Damageable Damageable => damageable;
        public UnitMovement Movement => unitMovement;
        public Selectable Selectable => selectable;
        public Animator Animator => animator;
        public AudioSource AudioSource => audioSource;

        public Knibling BaseObject => GetComponentInParent<Knibling>();

        #endregion

        public bool IsAggressive
        {
            get => isAggressive;
            set
            {
                currentState = value ? idleAggressiveState : idlePassiveState;
                isAggressive = value;
            }
        }

        public event System.Action UnitKilled;

        private void OnEnable()
        {
            damageable.Killed += OnKilled;
            damageable.Damaged += OnDamaged;
        }

        private void OnDisable()
        {
            damageable.Killed -= OnKilled;
            damageable.Damaged -= OnDamaged;
        }

        private void OnDamaged()
        {
            if (currentState == idlePassiveState) TransitionToState(idleAggressiveState);
        }

        private void OnKilled(Damageable unit)
        {
            TransitionToState(deadState);
            isActive = false;
            Movement.NavMeshAgent.isStopped = true;
            Selectable.allowSelection = false;
            damageCollider.enabled = false;

            SelectedObjects.Deselect(selectable);
            UnitKilled?.Invoke();
        }
    }
}