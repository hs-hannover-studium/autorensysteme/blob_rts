﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Decisions/IsDamageableAlive")]
    public class IsDamageableAliveDecision : Decision
    {
        private readonly int _idleAnimID = Animator.StringToHash("Idle");
        private readonly int _speedAnimID = Animator.StringToHash("Speed");

        public override bool Decide(StateController controller)
        {
            return IsAlive((StateControllerUnit) controller);
        }

        private bool IsAlive(StateControllerUnit controller)
        {
            bool targetIsAlive = controller.currentTarget && controller.currentTarget.IsAlive;

            if (!targetIsAlive)
            {
                controller.currentTarget = null;
                controller.moveDestination = controller.absoluteTarget
                    ? controller.absoluteTarget.transform.position
                    : Vector3.zero;
                controller.Movement.NavMeshAgent.isStopped = true;
                controller.Animator.SetFloat(_speedAnimID, 0);
                controller.Animator.SetTrigger(_idleAnimID);
            }

            return targetIsAlive;
        }
    }
}