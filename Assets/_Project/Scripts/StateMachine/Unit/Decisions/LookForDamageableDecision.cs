﻿using System.Collections.Generic;
using Kniblings.Combat;
using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Decisions/LookForDamageable")]
    public class LookForDamageableDecision : Decision
    {
        public LayerMask obstacleMask;
        public LayerMask targetMask;

        public override bool Decide(StateController controller)
        {
            return FindVisibleTargets((StateControllerUnit) controller);
        }

        /// <summary>
        ///     Sets first visible enemy as chase target
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        private bool FindVisibleTargets(StateControllerUnit controller)
        {
            Collider[] hitColliders =
                Physics.OverlapSphere(controller.transform.position, controller.UnitData.viewRange, targetMask);
            List<Damageable> visibleTargets = new List<Damageable>();

            foreach (Collider hitCollider in hitColliders)
            {
                if (!hitCollider.TryGetComponent(out Damageable damageable) || !damageable.IsAlive)
                    continue;

                float distance = Vector3.Distance(controller.transform.position, damageable.transform.position);

                // Check if view is blocked by obstacle
                if (Physics.Raycast(controller.transform.position + new Vector3(0, 0.5f, 0),
                    damageable.transform.position - controller.transform.position, out RaycastHit hit,
                    distance, obstacleMask))
                {
                    // Can't see
                    Debug.DrawRay(controller.transform.position + new Vector3(0, 0.5f, 0),
                        damageable.transform.position - controller.transform.position, Color.red, 0.5f);
                    continue;
                }

                // Can see
                Debug.DrawRay(controller.transform.position + new Vector3(0, 0.5f, 0),
                    damageable.transform.position - controller.transform.position, Color.yellow, 0.5f);

                visibleTargets.Add(damageable);
            }

            if (visibleTargets.Count <= 0)
            {
                return false;
            }

            controller.currentTarget = FindClosestTarget(visibleTargets, controller);
            return true;
        }

        /// <summary>
        ///     Selects the closest target
        /// </summary>
        /// <param name="targets"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        private Damageable FindClosestTarget(List<Damageable> targets, StateControllerUnit controller)
        {
            Vector3 pos = controller.transform.position;
            Damageable closestTarget = targets.Count > 0 ? targets[0] : null;

            foreach (Damageable target in targets)
            {
                float distanceA = Vector3.Distance(pos, closestTarget.transform.position);
                float distanceB = Vector3.Distance(pos, target.transform.position);

                if (distanceA > distanceB)
                    closestTarget = target;
            }

            if (closestTarget == null)
                return null;

            Debug.DrawRay(controller.transform.position + new Vector3(0, 0.5f, 0),
                closestTarget.transform.position - controller.transform.position, Color.green, 0.5f);

            return closestTarget;
        }
    }
}