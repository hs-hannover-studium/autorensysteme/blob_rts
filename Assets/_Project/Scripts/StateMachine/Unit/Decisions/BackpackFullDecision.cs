﻿using Kniblings.Controllers;
using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{

    [CreateAssetMenu(fileName = nameof(BackpackFullDecision), menuName = "StateMachine/Unit/Decisions/BackpackFull",
        order = 0)]
    public class BackpackFullDecision : Decision
    {
        public override bool Decide(StateController controller)
        {
            return BackpackFull((StateControllerVillager) controller);
        }

        private bool BackpackFull(StateControllerVillager controller)
        {
            if (controller.backpack.GetTotal() < controller.VillagerData.backpackCapacity) 
                return false;
            
            // Move to village core to deliver resources and clear currentResource
            controller.moveDestination = PlayerController.Instance.villageCore.position;
            controller.currentResourceTarget.FoundBy = null;
            controller.currentResourceTarget = null;
            return true;
        }
    }
}