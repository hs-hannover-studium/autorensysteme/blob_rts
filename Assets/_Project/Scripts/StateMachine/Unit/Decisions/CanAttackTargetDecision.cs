﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Decisions/CanAttackTarget")]
    public class CanAttackTargetDecision : Decision
    {
        public LayerMask obstacleMask;
        public LayerMask targetMask;

        public override bool Decide(StateController controller)
        {
            return CanAttack((StateControllerUnit) controller);
        }

        private bool CanAttack(StateControllerUnit controller)
        {
            if (!controller.currentTarget || !controller.currentTarget.IsAlive)
                return false;

            if (!Physics.Raycast(controller.transform.position + new Vector3(0, 0.5f, 0),
                controller.currentTarget.transform.position - controller.transform.position, out RaycastHit hitInfo,
                controller.UnitData.viewRange, targetMask))
                return false;

            float distance = Vector3.Distance(controller.transform.position, hitInfo.point);

            if (distance > controller.UnitData.attackRange)
                return false;

            // Check if view is blocked by obstacle
            if (Physics.Raycast(controller.transform.position + new Vector3(0, 0.5f, 0),
                controller.currentTarget.transform.position - controller.transform.position, out RaycastHit hit,
                distance, obstacleMask))
                return false;

            return true;
        }
    }
}