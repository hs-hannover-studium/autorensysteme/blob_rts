﻿using System.Collections.Generic;
using Kniblings.Locomotion;
using Kniblings.World;
using Sirenix.Utilities;
using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(fileName = nameof(LookForResourceCollectibleDecision), menuName = "StateMachine/Unit/Decisions/LookForResourceCollectible", order = 0)]
    public class LookForResourceCollectibleDecision : Decision
    {
        public LayerMask targetMask;
        public int maxColliderCount = 32;
        public int minColliderCount = 4;

        public override bool Decide(StateController controller)
        {
            return FindClosestResourceCollectible((StateControllerVillager) controller);
        }

        private bool FindClosestResourceCollectible(StateControllerVillager controller)
        {
            Vector3 pos = controller.transform.position;

            // Find all colliders on target layer mask, increasing the view range gradually if nothing is found 
            Collider[] hitColliders = new Collider[maxColliderCount];
            for (int i = 4; i >= 1 ; i--)
            {
                int count = Physics.OverlapSphereNonAlloc(pos, controller.UnitData.viewRange / i, hitColliders, targetMask);
                
                // break if there are enough colliders found
                // should be more than zero because the resource may already be blocked by another knibling (see line 50 "FoundBy")
                if (count > minColliderCount)
                    break;
            }

            List<ResourceCollectible> foundResources = new List<ResourceCollectible>();

            foreach (Collider hitCollider in hitColliders)
            {
                // This check is needed because the array may not be full
                if (hitCollider == null)
                {
                    break;
                }
                
                if (!hitCollider.TryGetComponent(out ResourceCollectible resource))
                    continue;
                
                if (resource.FoundBy & resource.FoundBy != controller)
                {
                    Debug.DrawLine(pos + new Vector3(0, 0.5f, 0), resource.transform.position,
                        Color.red * 0.7f, 0.5f);
                    continue;
                }
                
                Debug.DrawLine(pos + new Vector3(0, 0.5f, 0), hitCollider.transform.position,
                    Color.yellow * 0.5f, 0.5f);
                
                foundResources.Add(resource);
            }

            if (foundResources.Count <= 0)
            {
                // UnitMovement.RandomPosInsideSphere(controller.UnitData.viewRange, pos, out Vector3 location);
                // controller.moveDestination = location;
                return false;
            }

            // Find the closest of the found resources and set the variable in the controller
            ResourceCollectible closestResource = foundResources[0];
            foreach (ResourceCollectible resource in foundResources)
            {
                
                
                float a = Vector3.Distance(pos, closestResource.transform.position);
                float b = Vector3.Distance(pos, resource.transform.position);
                
                if (a > b)
                    closestResource = resource;
            }
            
            Debug.DrawLine(pos + new Vector3(0, 0.5f, 0), closestResource.transform.position,
                Color.yellow, 0.5f);

            closestResource.FoundBy = controller;
            // Debug.Log($"{closestResource.gameObject.name} found by {controller.gameObject.name}", controller);
            controller.currentResourceTarget = closestResource;
            controller.moveDestination = closestResource.transform.position;
            return true;
        }
    }
}
