﻿using UnityEngine;
using UnityEngine.AI;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Decisions/ReachedDestination")]
    public class ReachedDestinationDecision : Decision
    {
        private readonly int _speedAnimID = Animator.StringToHash("Speed");

        public override bool Decide(StateController controller)
        {
            return ReachedDestination((StateControllerUnit) controller);
        }

        protected bool ReachedDestination(StateControllerUnit controller)
        {
            NavMeshAgent navMeshAgent = controller.Movement.NavMeshAgent;

            if (navMeshAgent.pathPending)
                return false;

            bool reached = navMeshAgent.remainingDistance <=
                           navMeshAgent.stoppingDistance;

            if (reached)
            {
                controller.Animator.SetFloat(_speedAnimID, 0);
                controller.moveDestination = Vector3.zero;
            }

            return reached;
        }
    }
}