﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(fileName = nameof(ReachedResourceDecision), menuName = "StateMachine/Unit/Decisions/ReachedResource", order = 0)]
    public class ReachedResourceDecision : ReachedDestinationDecision
    {
        public override bool Decide(StateController controller)
        {
            return ReachedResource((StateControllerVillager) controller);
        }

        private bool ReachedResource(StateControllerVillager controller)
        {
            if (!ReachedDestination(controller))
                return false;
            controller.currentResourceTarget.PickUp(controller);
            return true;
        }
    }
}