﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(fileName = nameof(IdleTimeOverDecision), menuName = "StateMachine/Unit/Decisions/IdleTimeOver")]
    public class IdleTimeOverDecision : Decision
    {
        public float idleTime;
        
        public override bool Decide(StateController controller)
        {
            return IdleTimeOver(controller);
        }

        private bool IdleTimeOver(StateController controller)
        {
            float start = controller.IdleStartTime;
            if (Time.time - start < idleTime) return false;
            
            // Set start time to zero so that nothing breaks if this Decision is called without setting the start time first
            controller.IdleStartTime = 0;
            return true;
        }
    }
}