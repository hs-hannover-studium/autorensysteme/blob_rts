﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(fileName = nameof(ResourceDestroyedDecision), menuName = "StateMachine/Unit/Decisions/ResourceDestroyed", order = 0)]
    public class ResourceDestroyedDecision : Decision
    {

        public override bool Decide(StateController controller)
        {
            return IsResourceDestroyed((StateControllerVillager) controller);
        }

        private bool IsResourceDestroyed(StateControllerVillager controller)
        {
            return controller.currentResourceTarget == null;
        }
    }
}
