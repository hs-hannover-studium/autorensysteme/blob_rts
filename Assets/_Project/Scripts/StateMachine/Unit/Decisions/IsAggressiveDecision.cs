﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Decisions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Decisions/IsAggressive")]
    public class IsAggressiveDecision : Decision
    {
        public override bool Decide(StateController controller)
        {
            StateControllerUnit stateControllerUnit = (StateControllerUnit) controller;
            return stateControllerUnit.IsAggressive;
        }
    }
}