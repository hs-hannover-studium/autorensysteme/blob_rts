﻿using Kniblings.Data;
using Kniblings.World;
using Sirenix.OdinInspector;

namespace Kniblings.StateMachine.Unit
{
    public class StateControllerVillager : StateControllerUnit
    {
        [BoxGroup("Resources"), ReadOnly] public ResourcePackage backpack;
        [BoxGroup("Resources"), ReadOnly] public ResourceCollectible currentResourceTarget;

        public VillagerData VillagerData => (VillagerData) UnitData;
        
        
    }
}