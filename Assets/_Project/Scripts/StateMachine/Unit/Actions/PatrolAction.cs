﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Actions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Actions/Patrol")]
    public class PatrolAction : Action
    {
        public override void Act(StateController controller)
        {
            Patrol(controller);
        }

        private void Patrol(StateController controller)
        {
            Debug.Log("Patrol Action");
        }
    }
}