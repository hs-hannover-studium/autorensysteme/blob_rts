﻿using Kniblings.Controllers;
using Kniblings.Data;
using UnityEngine;

namespace Kniblings.StateMachine.Unit.Actions
{
    [CreateAssetMenu(fileName = nameof(DeliverResourcesAction), menuName = "StateMachine/Unit/Actions/DeliverResources", order = 0)]            
    public class DeliverResourcesAction : Action
    {
        public override void Act(StateController controller)
        {
            DeliverResources((StateControllerVillager) controller);
        }

        private void DeliverResources(StateControllerVillager controller)
        {
            ResourcePackage res = controller.backpack;
            PlayerController.Instance.AddResources(res);
            controller.backpack = new ResourcePackage();
        }
    }
}