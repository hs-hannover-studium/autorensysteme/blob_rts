﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Actions
{
    [CreateAssetMenu(fileName = nameof(SetIdleStartTimeAction), menuName = "StateMachine/Unit/Actions/SetIdleStartTime")]
    public class SetIdleStartTimeAction : Action
    {
        public override void Act(StateController controller)
        {
            controller.IdleStartTime = Time.time;
        }
    }
}