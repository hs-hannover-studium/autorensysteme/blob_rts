﻿using UnityEngine;

namespace Kniblings.StateMachine.Unit.Actions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Actions/Attack")]
    public class AttackAction : Action
    {
        private readonly int _speedAnimID = Animator.StringToHash("Speed");

        public override void Act(StateController controller)
        {
            Attack((StateControllerUnit) controller);
        }

        private void Attack(StateControllerUnit controller)
        {
            if (!controller.currentTarget || !controller.currentTarget.IsAlive)
                return;

            if (!(Time.time >= controller.AttackBase.NextAttackTime))
                return;

            controller.Animator.SetFloat(_speedAnimID, 0);

            controller.AttackBase.Attack(controller.currentTarget);
        }
    }
}