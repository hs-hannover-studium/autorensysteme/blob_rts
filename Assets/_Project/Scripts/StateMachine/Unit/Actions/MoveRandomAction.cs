﻿using Kniblings.Locomotion;
using UnityEngine;

namespace Kniblings.StateMachine.Unit.Actions
{
    [CreateAssetMenu(fileName = nameof(MoveRandomAction), menuName = "StateMachine/Unit/Actions/MoveRandom")]
    public class MoveRandomAction : MoveAction
    {
        public override void Act(StateController controller)
        {
            MoveRandom((StateControllerUnit) controller);
        }

        private void MoveRandom(StateControllerUnit controller)
        {
            UnitMovement.RandomPosInsideSphere(controller.UnitData.viewRange, controller.transform.position,
                out Vector3 location);
            controller.moveDestination = location;
            Move(controller);

        }
    }
}