﻿using Kniblings.Locomotion;
using UnityEngine;
using UnityEngine.AI;

namespace Kniblings.StateMachine.Unit.Actions
{
    [CreateAssetMenu(menuName = "StateMachine/Unit/Actions/Move")]
    public class MoveAction : Action
    {
        public override void Act(StateController controller)
        {
            Move((StateControllerUnit) controller);
        }

        protected void Move(StateControllerUnit controller)
        {
            int index = 0;
            int maxLoops = 1000;
            Vector3 dir = (controller.transform.position - controller.moveDestination).normalized;
            Vector3 location;

            while (index != maxLoops)
            {
                if (NavMesh.SamplePosition(controller.moveDestination + dir * (index * 0.25f), out NavMeshHit hit, 1f,
                    NavMesh.AllAreas))
                {
                    if (UnitMovement.RandomPosInsideSphere(0.10f, hit.position, out Vector3 randomLocation))
                        location = randomLocation;
                    else
                        location = hit.position;

                    controller.Movement.MoveTo(location);
                    Debug.DrawRay(location, Vector3.up, Color.yellow, 3f);

                    break;
                }

                // if (controller.Movement.RandomPosInsideSphere(index * 0.25f,controller.currentTarget.transform.position, out Vector3 randomLocation))
                // {
                //     controller.Movement.MoveTo(randomLocation);
                //     Debug.DrawRay(randomLocation, Vector3.up, Color.yellow, 3f);
                //     break;
                // }

                index++;
            }
        }
    }
}