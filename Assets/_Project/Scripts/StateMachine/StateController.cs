﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.StateMachine
{
    [DisallowMultipleComponent]
    public abstract class StateController : MonoBehaviour
    {
        #region Variables

        [BoxGroup("StateMachine")] public State currentState;

        [BoxGroup("StateMachine")] public State remainState;

        [BoxGroup("StateMachine")] public bool isActive = true;

        private readonly int interval = 10;

        public float IdleStartTime { get; set; }

        #endregion
        
        protected virtual void Start()
        {
            currentState.OnStateEnter(this);
        }

        private void Update()
        {
            if (Time.frameCount % interval == 0 && isActive)
                // Debug.Log("Frame Count");
                UpdateState2();
        }

        public void TransitionToState(State nextState)
        {
            if (nextState == remainState || !isActive)
                return;
            
            currentState.OnStateExit(this);
            currentState = nextState;
            currentState.OnStateEnter(this);
        }

        protected void UpdateState2()
        {
            currentState.UpdateState(this);
        }
    }
}