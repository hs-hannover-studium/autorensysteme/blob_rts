﻿using UnityEngine;

namespace Kniblings.StateMachine
{
    /// <summary>
    ///     Used to decide wether a state should be changed or not.
    ///     Used in of transitions.
    /// </summary>
    public abstract class Decision : ScriptableObject
    {
        public abstract bool Decide(StateController controller);
    }
}