﻿using UnityEngine;

namespace Kniblings.StateMachine
{
    /// <summary>
    ///     Class which is used inside of states.
    /// </summary>
    public abstract class Action : ScriptableObject
    {
        public abstract void Act(StateController controller);
    }
}