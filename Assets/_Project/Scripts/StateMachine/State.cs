﻿using UnityEngine;

namespace Kniblings.StateMachine
{
    /// <summary>
    ///     Class which contains all actions and transitions to next state, either true or false state, based on
    ///     transitions.
    /// </summary>
    [CreateAssetMenu(menuName = "StateMachine/Unit/State")]
    public class State : ScriptableObject
    {
        [Tooltip("Actions that get called every fixed update")]
        public Action[] actions;

        [Tooltip("Action that gets called only once on state enter")]
        public Action onEnterAction;

        [Tooltip("Action that gets called only once on state exit")]
        public Action onExitAction;

        [Tooltip("Transitions that get called every fixed update")]
        public Transition[] transitions;

        /// <summary>
        ///     Called on State Enter
        /// </summary>
        /// <param name="controller"></param>
        public void OnStateEnter(StateController controller)
        {
            if (!onEnterAction)
                return;
            onEnterAction.Act(controller);
        }

        /// <summary>
        ///     Called on State Exit
        /// </summary>
        /// <param name="controller"></param>
        public void OnStateExit(StateController controller)
        {
            if (!onExitAction)
                return;
            onExitAction.Act(controller);
        }

        public void UpdateState(StateController controller)
        {
            DoActions(controller);
            CheckTransitions(controller);
        }

        /// <summary>
        ///     Execute Act on each Action
        /// </summary>
        /// <param name="controller"></param>
        private void DoActions(StateController controller)
        {
            foreach (Action action in actions) action.Act(controller);
        }

        /// <summary>
        ///     Checks each transitions if their decisions succeeded or not and switches states based on their return value
        /// </summary>
        /// <param name="controller"></param>
        private void CheckTransitions(StateController controller)
        {
            foreach (Transition transition in transitions)
            {
                bool decisionSucceeded = transition.decision.Decide(controller);

                controller.TransitionToState(decisionSucceeded ? transition.trueState : transition.falseState);
            }
        }
    }
}