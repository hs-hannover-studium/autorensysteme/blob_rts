﻿#if UNITY_EDITOR

using System;
using Kniblings.CustomInput;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Kniblings.Utility
{
    /// <summary>
    ///     This is currently Editor only but could be adapted to be usable in build.
    /// </summary>
    public class CameraCapture : MonoBehaviour
    {
        [SerializeField] private int resolutionFactor;

        [SerializeField, 
         ReadOnly, 
         HideIf("@string.IsNullOrWhiteSpace(_path)")]
        private string _path = "";


        private void OnEnable()
        {
            InputController.Actions.debug1.Down += TakeScreenshot;
        }


        private void OnDisable()
        {
            InputController.Actions.debug1.Down -= TakeScreenshot;
        }

        [Button("Capture Screen and save")]
        public void TakeScreenshot()
        {
            string path;
            if (string.IsNullOrWhiteSpace(_path))
            {
                path = EditorUtility.SaveFilePanel("Save Screenshot as PNG", "",
                    $"screenshot_{DateTime.Now:yyyy-MM-dd_HHmmss}", "png");
                if (string.IsNullOrWhiteSpace(path)) return;
            }
            else
            {
                path = _path + $"screenshot_{DateTime.Now:yyyy-MM-dd_HHmmss}.png";
            }

            ScreenCapture.CaptureScreenshot(path, resolutionFactor);
            Debug.Log($"Saved screenshot to: {path}");
        }

        [Button]
        public void SetPath()
        {
            string path = EditorUtility.OpenFolderPanel("Set screenshot folder", "", "");
            
            if (string.IsNullOrWhiteSpace(path)) return;

            Debug.Log(path);
            _path = path + "/";
        }

        [Button, HideIf("@string.IsNullOrWhiteSpace(_path)")]
        public void ClearPath()
        {
            _path = null;
        }
        
    }
}

#endif