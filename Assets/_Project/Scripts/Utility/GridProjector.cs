﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace Kniblings.Utility
{
    public class GridProjector : MonoBehaviour
    {
        private Transform[] _hexTiles;

        public float angleTolerance = 5f;
        public LayerMask layerMask;

        [MinValue(0)] [MaxValue(7)] public int minimumVertexMatches;

        public Vector3 snapOffset;


        [Button]
        public void ProjectTilesToMesh()
        {
            Undo.RegisterFullObjectHierarchyUndo(gameObject, "Project Grid");
            _hexTiles = GetChildrenArray(transform);
            foreach (Transform tile in _hexTiles)
            {
                MeshFilter mf = tile.GetComponent<MeshFilter>();
                Vector3[] vertices = mf.sharedMesh.vertices;

                int matches = 0;

                List<Vector3> hitPoints = new List<Vector3>(7);

                bool first = true;
                Vector3 center = Vector3.zero;
                foreach (Vector3 vertex in vertices)
                {
                    Vector3 vertexWorldPosition = vertex + tile.position;
                    Ray ray = new Ray(vertexWorldPosition, Vector3.down);


                    if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask))
                    {
                        // break if even one vertex is out of bounds
                        matches = -1;
                        break;
                    }

                    hitPoints.Add(hit.point);
                    if (first)
                    {
                        Debug.DrawRay(vertexWorldPosition, Vector3.down * 100, Color.magenta, 0.2f);
                        center = hit.point;
                        first = false;
                    }

                    float dot = Vector3.Dot(hit.normal.normalized, Vector3.up);

                    float angle = Mathf.Acos(dot);

                    // Debug.Log($"({tile.gameObject.name})  Dot: {dot},     Angle: {angle * Mathf.Rad2Deg}");

                    if (angle < angleTolerance * Mathf.Deg2Rad) matches++;
                }

                if (matches < minimumVertexMatches)
                {
                    DestroyImmediate(tile.gameObject);
                    continue;
                }

                tile.position = center + snapOffset;
            }
        }

        private Transform[] GetChildrenArray(Transform t)
        {
            List<Transform> tmp = new List<Transform>();
            foreach (Transform child in transform) tmp.Add(child);

            return tmp.ToArray();
        }
    }
}
#endif