﻿using TMPro;
using UnityEngine;

namespace Kniblings.Utility
{
    [DefaultExecutionOrder(-20)]
    public class BuildDebug : MonoBehaviour
    {
        private static TextMeshProUGUI _tmp;

        private void Awake()
        {
            _tmp = GetComponent<TextMeshProUGUI>();
            DontDestroyOnLoad(transform.parent.gameObject);
        }
        
        public static void Log(string msg)
        {
            _tmp.SetText(msg);
        }
        
        
        
    }
}
