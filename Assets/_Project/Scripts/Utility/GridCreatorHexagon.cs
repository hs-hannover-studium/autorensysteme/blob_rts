﻿using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Kniblings.Utility
{
#if UNITY_EDITOR
    public class GridCreatorHexagon : MonoBehaviour
    {
        private float _innerRadius;
        private float _outerRadius;
        private bool _xz;
        public int gridRadius;
        public GameObject gridTilePrefab;


        /// <summary>
        ///     Determine the necessary values from the mesh by examining the bounds
        ///     Also determines the orientation of the hexagon
        /// </summary>
        private void GetValuesFromMesh()
        {
            MeshRenderer mr = gridTilePrefab.GetComponent<MeshRenderer>();
            Vector3 ext = mr.bounds.extents;
            if (ext.x > ext.z)
            {
                _outerRadius = ext.x;
                _innerRadius = ext.z;
                _xz = true;
            }
            else
            {
                _outerRadius = ext.z;
                _innerRadius = ext.x;
                _xz = false;
            }
        }

        /// <summary>
        ///     Create the Hexagonal Grid out of the supplied prefab mesh
        ///     Use a simple nested for-loop for x and z axes, running from negative to positive gridRadius.
        ///     The Tile [0,0] will always be at the center this way ( at Position 0,0,0 )
        ///     Each tile gets moved to its position based on the calculations below, using the previously determined values
        ///     Every other row has to be offset by half a hexagon (_innerRadius), this is done using an alternating boolean
        /// </summary>
        /// <exception cref="Exception"></exception>
        [Button(ButtonSizes.Large, ButtonStyle.Box)]
        public void CreateGrid()
        {
            if (transform.childCount > 0)
                throw new Exception("Please clear children first!");

            GetValuesFromMesh();
            Vector3 origin = transform.position;
            bool evenRow = true;

            for (int x = -gridRadius; x <= gridRadius; x++)
            {
                for (int z = -gridRadius; z <= gridRadius; z++)
                {
                    GameObject tile = (GameObject) PrefabUtility.InstantiatePrefab(gridTilePrefab, transform);

                    tile.transform.position = origin + (_xz
                        ? new Vector3(x * _outerRadius * 1.5f, 0,
                            z * _innerRadius * 2f + (evenRow ? _innerRadius : 0))
                        : new Vector3(z * _innerRadius * 2f + (evenRow ? _innerRadius : 0), 0,
                            x * _outerRadius * 1.5f));

                    tile.name = $"HexTile[{x}, {z}]";
                }

                evenRow = !evenRow;
            }
        }

        [Button(ButtonSizes.Medium, ButtonStyle.Box)]
        public void ClearGrid()
        {
            while (transform.childCount > 0)
                DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }
#endif
}