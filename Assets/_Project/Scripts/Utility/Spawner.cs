﻿using Kniblings.Locomotion;
using Kniblings.Other;
using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.Utility
{
    public class Spawner : MonoBehaviour
    {
        /// <summary>
        ///     Spawns prefab at specific location
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="targetPosition"></param>
        public static void Spawn(GameObject prefab, Vector3 targetPosition)
        {
            GameObject unit = Instantiate(prefab, targetPosition, Quaternion.identity);
        }

        /// <summary>
        ///     Spawns a StateControllerUnit prefab at given targetPosition.
        ///     After spawning the Unit it will move to gatherPosition.
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="targetPosition"></param>
        /// <param name="gatherPosition"></param>
        /// <param name="randomDestinationRadius">Radius in which a random location will be picked</param>
        public static StateControllerUnit Spawn(StateControllerUnit prefab, Vector3 targetPosition,
            Vector3 gatherPosition, float randomDestinationRadius = 0f)
        {
            GameObject unit = Instantiate(prefab.gameObject, targetPosition, Quaternion.identity);
            StateControllerUnit controller = unit.GetComponent<StateControllerUnit>();

            if (!UnitMovement.RandomPosInsideSphere(randomDestinationRadius, gatherPosition,
                out Vector3 location))
                controller.moveDestination = gatherPosition;
            else
                controller.moveDestination = location;

            return controller;
        }

        public static Knibling Spawn(Knibling prefab, Vector3 spawnPosition, Vector3 gatherPosition,
            float randomRadius = 0f)
        {
            GameObject o = Instantiate(prefab.gameObject, spawnPosition, Quaternion.identity);
            Knibling knibling = o.GetComponent<Knibling>();

            knibling.Init(gatherPosition, randomRadius);
            return knibling;
        }
    }
}