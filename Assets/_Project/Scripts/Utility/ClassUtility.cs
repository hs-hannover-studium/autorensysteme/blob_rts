﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Kniblings.Utility
{
    public static class ClassUtility
    {
        public static T1[] TryGetFieldsOfType<T1, T2>(T2 obj)
        {
            List<T1> output = new List<T1>();

            FieldInfo[] fields = typeof(T2).GetFields();
            foreach (FieldInfo fieldInfo in fields)
                try
                {
                    T1 value = (T1) fieldInfo.GetValue(obj);
                    output.Add(value);
                }
                catch (InvalidCastException e)
                {
                    // ignored, field type not requested
                }

            return output.ToArray();
        }
    }
}