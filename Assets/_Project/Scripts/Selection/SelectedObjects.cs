﻿using System;
using System.Collections.Generic;
using Kniblings.Data;
using Kniblings.Data.Enums;

namespace Kniblings.Selection
{
    public static class SelectedObjects
    {
        public static SelectionType SelectionType;
        public static List<Selectable> Selected = new List<Selectable>();
        public static event Action<int> SelectionChanged;
        /// <summary>
        ///     Adds a selected entry
        /// </summary>
        /// <param name="s"></param>
        public static void Add(Selectable s)
        {
            if (Selected.Contains(s))
                return;

            Selected.Add(s);
            s.Select();
            SelectionType = s.Type;

            SelectionChanged?.Invoke(Selected.Count);
        }

        public static void Add(List<Selectable> selectables)
        {
            foreach (Selectable s in selectables)
            {
                int id = s.GetInstanceID();

                if (Selected.Contains(s))
                    return;

                Selected.Add(s);
                s.Select();
                SelectionType = s.Type;
            }

            SelectionChanged?.Invoke(Selected.Count);
        }

        /// <summary>
        ///     Deselect a single entry
        /// </summary>
        /// <param name="id"></param>
        public static void Deselect(Selectable s)
        {
            if (!Selected.Contains(s))
                return;

            s.Deselect();
            Selected.Remove(s);

            if (Selected.Count <= 0)
                ResetSelectionType();

            SelectionChanged?.Invoke(Selected.Count);
        }

        /// <summary>
        ///     Deselects all entries
        /// </summary>
        public static void DeselectAll()
        {
            if (Selected.Count == 0)
                return;

            foreach (Selectable s in Selected)
                try
                {
                    s.Deselect();
                }
                catch
                {
                    //ignore
                }

            ResetSelectionType();

            Selected.Clear();
            SelectionChanged?.Invoke(Selected.Count);
        }

        private static void ResetSelectionType()
        {
            SelectionType = SelectionType.None;
        }
    }
}