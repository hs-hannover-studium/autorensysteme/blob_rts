﻿using Kniblings.World.Buildings;
using UnityEngine;

namespace Kniblings.Selection
{
    public class SelectableBuilding : Selectable
    {
        private Material _baseMaterial;
        private BuildingContainer _container;

        private MeshRenderer _mr;
        public Material outlineMaterial;

        public BuildingBase BuildingBase { get; private set; }

        private void Awake()
        {
            _mr = GetComponent<MeshRenderer>();
            _baseMaterial = _mr.material;
            _container = transform.parent.GetComponent<BuildingContainer>();
            _container.BuildComplete += ContainerOnBuildComplete;
            BuildingBase = GetComponent<BuildingBase>();
        }

        private void ContainerOnBuildComplete()
        {
            allowSelection = true;
            _container.BuildComplete -= ContainerOnBuildComplete;
        }

        protected override void OnSelect()
        {
            _mr.materials = new[] {_baseMaterial, outlineMaterial};
        }

        protected override void OnDeselect()
        {
            _mr.materials = new[] {_baseMaterial};
        }
    }
}