﻿using System;
using Kniblings.Data;
using Kniblings.Data.Enums;
using UnityEngine;

namespace Kniblings.Selection
{
    public abstract class Selectable : MonoBehaviour
    {
        public bool allowSelection = true;
        [SerializeField] private bool isSelected;
        [SerializeField] private SelectionType type;

        public SelectionType Type => type;

        public bool IsSelected => isSelected;

        public event Action Selected;
        public event Action Deselected;

        private void Start()
        {
            if (isSelected)
            {
                Selected?.Invoke();
                OnSelect();
            }
        }

        public void Select()
        {
            if (!gameObject.activeSelf)
                return;

            isSelected = true;
            Selected?.Invoke();

            OnSelect();
        }

        protected abstract void OnSelect();

        public void Deselect()
        {
            if (!gameObject.activeSelf)
                return;

            isSelected = false;
            Deselected?.Invoke();

            OnDeselect();
        }

        protected abstract void OnDeselect();
    }
}