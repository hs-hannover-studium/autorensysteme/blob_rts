﻿using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.Selection
{
    public class SelectableUnit : Selectable
    {
        [SerializeField] private Material baseMaterial;
        [SerializeField] private Renderer r;
        [SerializeField] private Material selectionMaterial;

        public StateControllerUnit StateController { get; private set; }

        private void Awake()
        {
            StateController = GetComponent<StateControllerUnit>();
        }

        protected override void OnSelect()
        {
            r.materials = new[] {baseMaterial, selectionMaterial};
        }

        protected override void OnDeselect()
        {
            r.materials = new[] {baseMaterial};
        }
    }
}