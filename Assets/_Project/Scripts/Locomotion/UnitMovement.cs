﻿using Kniblings.Controllers;
using Kniblings.StateMachine.Unit;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace Kniblings.Locomotion
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class UnitMovement : MonoBehaviour
    {
        
        #region Variables

        [SerializeField] protected NavMeshAgent navMeshAgent;
        [SerializeField] protected StateControllerUnit controller;
        [SerializeField] protected AudioSource audioSourceRunning;

        protected float OriginalWalkSpeed;
        private readonly int _speedID = Animator.StringToHash("Speed");

        #endregion

        #region Properties

        public NavMeshAgent NavMeshAgent => navMeshAgent;
        public StateControllerUnit Controller => controller;

        public float WalkSpeed
        {
            get => navMeshAgent.speed;
            set => navMeshAgent.speed = value;
        }

        #endregion
        
        private void Start()
        {
            controller = controller ? controller : GetComponent<StateControllerUnit>();
            audioSourceRunning.clip = AudioController.Instance.unitMovement;

            navMeshAgent.acceleration = 999;
            navMeshAgent.stoppingDistance = Random.Range(0f, 0.3f);
            OriginalWalkSpeed = Controller.UnitData.walkSpeed - Random.Range(0f, 0.25f);
            WalkSpeed = OriginalWalkSpeed;

            navMeshAgent.updateRotation = false;
        }

        private void Update()
        {
            if (navMeshAgent.velocity != Vector3.zero)
            {
                if (!audioSourceRunning.isPlaying)
                {
                    audioSourceRunning.pitch = Random.Range(0.6f, 1f);
                    audioSourceRunning.Play();
                }
            }
            else
            {
                audioSourceRunning.Stop();
            }
        }

        private void LateUpdate()
        {
            if (navMeshAgent.velocity.sqrMagnitude > Mathf.Epsilon)
                transform.rotation = Quaternion.LookRotation(navMeshAgent.velocity.normalized);
        }

        /// <summary>
        ///     Moves given StateControllerUnit to given destination.
        ///     If no path is found Unit won't move.
        /// </summary>
        /// <param name="destination"></param>
        public void MoveTo(Vector3 destination)
        {
            navMeshAgent.isStopped = false;
            NavMeshPath path = new NavMeshPath();
            navMeshAgent.CalculatePath(destination, path);

            if (path.status != NavMeshPathStatus.PathComplete)
            {
                Debug.Log("No Path found!");
                return;
            }

            controller.Animator.SetFloat(_speedID, controller.Movement.NavMeshAgent.speed);
            navMeshAgent.SetPath(path);

            DrawPath(path);
        }

        /// <summary>
        ///     Finds a location to move to on all navmeshes around given desiredLocation
        /// </summary>
        /// <param name="desiredLocation">Desired target location</param>
        /// <param name="location">Actual target location</param>
        /// <param name="origin">Starting position</param>
        /// <param name="radius">Radius to look for a viable position</param>
        /// <returns></returns>
        public static bool FindLocation(Vector3 desiredLocation, out Vector3 location, Vector3 origin,
            float radius = 1f)
        {
            // Checks if agent can walk to desiredLocation with an
            // additional maxDistance radius around desiredLocation
            if (!NavMesh.SamplePosition(desiredLocation, out NavMeshHit hitNavmesh, radius, NavMesh.AllAreas))
            {
                Debug.DrawRay(desiredLocation, Vector3.up * 7, Color.red, 2.0f);

                location = Vector3.zero;
                return false;
            }

            // Checks distance to exclude locations that are behind walls or other strange locations
            float navDistance = GetPathDistance(origin, hitNavmesh.position);
            float directDistance = Vector3.Distance(origin, hitNavmesh.position);

            if (navDistance > directDistance + directDistance / 100 * 15)
            {
                Debug.DrawRay(desiredLocation, Vector3.up * 7, Color.red, 2.0f);

                location = Vector3.zero;
                return false;
            }

            Debug.DrawRay(hitNavmesh.position, Vector3.up, Color.green, 2.0f);
            location = hitNavmesh.position;
            return true;
        }

        /// <summary>
        ///     Looks for random movement location around given target with given radius
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="target"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public static bool RandomPosInsideSphere(float radius, Vector3 target, out Vector3 location)
        {
            Vector3 randomDirection = Random.insideUnitSphere * radius;
            randomDirection += target;

            if (!NavMesh.SamplePosition(randomDirection, out NavMeshHit hit, radius, NavMesh.AllAreas))
            {
                location = target;
                return false;
            }

            location = hit.position;
            return true;
        }

        /// <summary>
        ///     Sets rotation of transform to look at given target
        /// </summary>
        /// <param name="target"></param>
        public void FaceTarget(Transform target)
        {
            transform.LookAt(target);
        }

        public void ResetWalkSpeed()
        {
            WalkSpeed = OriginalWalkSpeed;
        }

        /// <summary>
        ///     Calculates navmesh path between two given Vector3 locations
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static float GetPathDistance(Vector3 source, Vector3 target)
        {
            NavMeshPath path = new NavMeshPath();

            NavMesh.CalculatePath(source, target, NavMesh.AllAreas, path);

            return GetPathDistance(path);
        }

        public static float GetPathDistance(NavMeshPath path)
        {
            float distance = 0;

            for (int i = 0; i < path.corners.Length - 1; i++)
                distance += Vector3.Distance(path.corners[i], path.corners[i + 1]);

            return distance;
        }

        public static void DrawPath(NavMeshPath path)
        {
            for (int i = 0; i < path.corners.Length - 1; i++)
                Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.magenta, 0.5f);
        }

    }

#if UNITY_EDITOR
    [CustomEditor(typeof(UnitMovement), true)]
    public class UnitMovementEditor : Editor
    {
        public void OnSceneGUI()
        {
            UnitMovement unitMovement = (UnitMovement) target;
            Transform t = unitMovement.transform;
            // Attack range
            Handles.color = Color.yellow;
            Handles.DrawWireArc(t.position, Vector3.up, Vector3.forward, 360,
                unitMovement.Controller.UnitData.viewRange);
        }
    }
#endif
}