﻿using UnityEngine;

namespace Kniblings.Locomotion
{
    public class ProjectileMove : MonoBehaviour
    {
        [SerializeField] private GameObject hitVfxPrefab;
        [SerializeField] private GameObject projectileVisuals;
        [SerializeField] private float speed;

        // Update is called once per frame
        private void Update()
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }

        private void OnCollisionEnter(Collision other)
        {
            speed = 0;
            projectileVisuals.SetActive(false);

            ContactPoint contact = other.contacts[0];
            Quaternion rotation = Quaternion.FromToRotation(Vector3.forward, contact.normal);
            Vector3 pos = contact.point;

            GameObject hitVfx = Instantiate(hitVfxPrefab, pos, rotation);

            if (hitVfx.TryGetComponent(out ParticleSystem hitParticleSystem))
            {
                Destroy(hitVfx, hitParticleSystem.main.duration);
                Destroy(gameObject, hitParticleSystem.main.duration);
            }
            else
            {
                ParticleSystem particleSystemChild = hitVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitVfx, particleSystemChild.main.duration);
                Destroy(gameObject, particleSystemChild.main.duration);
            }
        }
    }
}