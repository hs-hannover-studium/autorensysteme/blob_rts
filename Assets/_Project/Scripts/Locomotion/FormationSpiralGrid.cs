﻿using Kniblings.Selection;
using Kniblings.StateMachine;
using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.Locomotion
{
    public class FormationSpiralGrid : Formation
    {
        [Range(10, 50)] [SerializeField] private int loopLimit = 50;
        [SerializeField] private State moveState;
        [SerializeField] private float randomSpreadRadius;

        /*
         * Moves units in a spiral like way
         * ..  14 13  12
         *  4   3  2  11
         *  5   O  1  10
         *  6   7  8   9
         * Source: https://stackoverflow.com/questions/3706219/algorithm-for-iterating-over-an-outward-spiral-on-a-discrete-2d-grid-from-the-or
         */
        public override void Move(RaycastHit destination)
        {
            // Origin
            float ox = destination.point.x;
            float oz = destination.point.z;
            Debug.DrawRay(new Vector3(ox, destination.point.y, oz), Vector3.up * 2, Color.blue, 2.0f);

            // current position (x, y) (viewed from 2 dimensions)
            float x = ox;
            float y = oz;

            // (dx, dy) is a vector - direction in which we move right now
            float dx = spaceBetweenUnits;
            float dy = 0f;

            int segmentLength = 1;
            int segmentPassed = 0;

            foreach (Selectable s in SelectedObjects.Selected)
            {
                bool foundLocation = false;
                int loopCount = 0;

                if (!s.gameObject.TryGetComponent(out StateControllerUnit controller))
                    return;

                while (!foundLocation)
                {
                    if (loopCount >= loopLimit)
                    {
                        Debug.Log("Can't find a location to move to!", controller);
                        break;
                    }

                    if (UnitMovement.FindLocation(new Vector3(x, destination.point.y, y), out Vector3 location,
                        destination.point, 2.5f))
                    {
                        if (UnitMovement.RandomPosInsideSphere(randomSpreadRadius, location,
                            out Vector3 randomLocation))
                            controller.moveDestination = randomLocation;
                        else
                            controller.moveDestination = location;

                        controller.TransitionToState(controller.UnitData.defaultMoveState);
                        foundLocation = true;
                    }

                    // make a step, add 'direction' vector (dx, dy) to current position (x, y)
                    x += dx;
                    y += dy;

                    segmentPassed++;

                    if (segmentPassed == segmentLength)
                    {
                        // done with current segment
                        segmentPassed = 0;

                        // 'rotate' directions
                        float buffer = dx;
                        dx = -dy;
                        dy = buffer;

                        // increase segment length if necessary
                        if (dy == 0) segmentLength++;
                    }

                    loopCount++;
                }
            }
        }
    }
}