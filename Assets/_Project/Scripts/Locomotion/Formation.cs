﻿using UnityEngine;

namespace Kniblings.Locomotion
{
    public abstract class Formation : MonoBehaviour
    {
        [Tooltip("Describes the range in which to look for a location to move to around desired location")]
        [Range(1f, 5.0f)]
        [SerializeField]
        protected float lookLocationRange = 2.5f;

        [Range(0, 100)] [SerializeField] private float movementTolerance = 15.0f;
        [Range(0.25f, 3.0f)] [SerializeField] protected float spaceBetweenUnits = 1.0f;

        /// <summary>
        ///     Move units to provided Raycast Hit
        /// </summary>
        /// <param name="destination"></param>
        public abstract void Move(RaycastHit destination);
    }
}