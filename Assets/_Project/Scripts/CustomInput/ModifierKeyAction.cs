﻿using System;
using UnityEngine;

namespace Kniblings.CustomInput
{
    [Serializable]
    public class ModifierKeyAction : KeyAction
    {
        private KeyAction _siblingKeybind;
        public KeyCode modifier = KeyCode.LeftShift;

        public override void Evaluate()
        {
            if (Input.GetKeyDown(modifier))
                SetSiblingBlock(true);

            if (Input.GetKey(modifier))
            {
                if (Input.GetKeyDown(key)) DownCall();

                if (Input.GetKeyUp(key)) UpCall();
            }

            if (Input.GetKeyUp(modifier))
                SetSiblingBlock(false);
        }

        public void FindSiblingReference(KeyAction[] possibleSiblings)
        {
            foreach (KeyAction possibleSibling in possibleSiblings)
            {
                if (possibleSibling.key != key)
                    continue;

                _siblingKeybind = possibleSibling;
                return;
            }
        }

        private void SetSiblingBlock(bool value)
        {
            try
            {
                _siblingKeybind.SetBlocked(value);
            }
            catch (NullReferenceException e)
            {
                // ignore, no sibling set
            }
        }
    }
}