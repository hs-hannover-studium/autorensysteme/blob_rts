﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Kniblings.CustomInput
{
    [CreateAssetMenu(fileName = "KeycodeNamesData", menuName = "Data/KeycodeNames", order = 0)]
    public class KeyCodeNamesData : SerializedScriptableObject
    {
        [OdinSerialize] public Dictionary<KeyCode, string> keyNames = new Dictionary<KeyCode, string>();

        [Button(ButtonSizes.Medium, Name = "Clear and Fill")]
        public void FillDictionary()
        {
            keyNames.Clear();
            foreach (KeyCode keyCode in Enum.GetValues(typeof(KeyCode)))
                try
                {
                    keyNames.Add(keyCode, Enum.GetName(typeof(KeyCode), keyCode));
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
        }

        public bool TryGetKeyCodeName(KeyCode keyCode, out string keyName)
        {
            return keyNames.TryGetValue(keyCode, out keyName);
        }
    }
}