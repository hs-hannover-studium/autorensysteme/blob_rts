﻿using System;
using UnityEngine;

namespace Kniblings.CustomInput
{
    [Serializable]
    public class SimpleKeyAction : KeyAction
    {
        public override void Evaluate()
        {
            if (Input.GetKeyDown(key)) DownCall();

            if (Input.GetKeyUp(key)) UpCall();
        }
    }
}