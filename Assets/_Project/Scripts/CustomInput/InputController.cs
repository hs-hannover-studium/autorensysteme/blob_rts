﻿using Kniblings.Utility;
using UnityEngine;

namespace Kniblings.CustomInput
{
    public sealed class InputController : MonoBehaviour
    {
        private KeyAction[] _keyActions;
        private ModifierKeyAction[] _modifierKeyActions;

        private SimpleKeyAction[] _simpleKeyActions;
        [SerializeField] private KeyActionData keyActionData;
        [SerializeField] private KeyCodeNamesData keyCodeNamesData;
        public static KeyActionData Actions { get; private set; }
        public static InputController Instance { get; private set; }


        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);

            Actions = keyActionData;
            _keyActions = ClassUtility.TryGetFieldsOfType<KeyAction, KeyActionData>(keyActionData);
            _simpleKeyActions = ClassUtility.TryGetFieldsOfType<SimpleKeyAction, KeyActionData>(keyActionData);
            _modifierKeyActions = ClassUtility.TryGetFieldsOfType<ModifierKeyAction, KeyActionData>(keyActionData);
            SiblingCheck();
        }

        private void SiblingCheck()
        {
            foreach (ModifierKeyAction modifierKeyAction in _modifierKeyActions)
                modifierKeyAction.FindSiblingReference(_simpleKeyActions);
        }

        private void Update()
        {
            foreach (KeyAction key in _keyActions) key.Evaluate();
        }

        public static Vector3 GetMousePosition()
        {
            return Input.mousePosition;
        }

        public static float GetMouseScrollDelta()
        {
            return Input.mouseScrollDelta.y;
        }
    }
}