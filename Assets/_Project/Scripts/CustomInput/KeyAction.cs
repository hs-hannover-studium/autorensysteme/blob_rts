﻿using System;
using UnityEngine;

namespace Kniblings.CustomInput
{
    [Serializable]
    public abstract class KeyAction
    {
        public string descriptiveName;
        public KeyCode key;

        public bool IsPressed { get; private set; }

        public bool IsBlocked { get; private set; }

        public event Action Down;
        public event Action Up;

        public abstract void Evaluate();

        protected void DownCall()
        {
            if (IsBlocked)
                return;

            Down?.Invoke();
            IsPressed = true;
        }

        protected void UpCall()
        {
            Up?.Invoke();
            IsPressed = false;
        }

        public void SetBlocked(bool value)
        {
            IsBlocked = value;
        }
    }
}