﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace Kniblings.CustomInput
{
    [CreateAssetMenu(fileName = "KeybindsData", menuName = "Data/Kebinds", order = 0)]
    public class KeyActionData : ScriptableObject
    {

        [Title("CameraMovement")] 
        public SimpleKeyAction cameraForward;
        public SimpleKeyAction cameraBackwards;
        public SimpleKeyAction cameraLeft;
        public SimpleKeyAction cameraRight;
        
        public SimpleKeyAction cameraRotateLeft;
        public SimpleKeyAction cameraRotateRight;

        public SimpleKeyAction cameraZoomIn;
        public SimpleKeyAction cameraZoomOut;

        public SimpleKeyAction focusOnVillageCore;


        [Title("DEBUG")] 
        public ModifierKeyAction debug1;
        public ModifierKeyAction debug2;

        [Title("Mouse Buttons")] 
        public SimpleKeyAction leftMouse;
        public SimpleKeyAction middleMouse;
        public SimpleKeyAction rightMouse;
        public ModifierKeyAction lockCursor;

        [Title("Build Mode")] 
        public SimpleKeyAction toggleBuildMode;
        public ModifierKeyAction buildingRotateCCW; 
        public SimpleKeyAction buildingRotateCW;
        // all fields have to be of type "Keybinding"

        [Title("Game Controls")] public SimpleKeyAction togglePauseMenu;
    }
}