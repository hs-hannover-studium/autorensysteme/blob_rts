﻿using System;
using System.Collections.Generic;
using Kniblings.Other;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data.Values
{
    [CreateAssetMenu(fileName = "KniblingsValue", menuName = "Data/Values/Kniblings")]
    public class KniblingsValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private List<Knibling> defaultValue;
        [NonSerialized] [ShowInInspector] private List<Knibling> runtimeValue = new List<Knibling>();

        public event Action<List<Knibling>> ValueChanged;

        public List<Knibling> RuntimeValue => runtimeValue;

        public void Add(Knibling k)
        {
            RuntimeValue.Add(k);
            ValueChanged?.Invoke(RuntimeValue);
        }

        public void Remove(Knibling k)
        {
            RuntimeValue.Remove(k);
            ValueChanged?.Invoke(RuntimeValue);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }
    }
}