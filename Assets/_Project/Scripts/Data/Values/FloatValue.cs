﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data.Values
{
    [CreateAssetMenu(fileName = "FloatValue", menuName = "Data/Values/Float")]
    public class FloatValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private float defaultValue;
        [NonSerialized] [ShowInInspector] private float runtimeValue;

        public event Action<float> ValueChanged;
        
        public float RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            // runtimeValue = defaultValue;
        }
    }
}