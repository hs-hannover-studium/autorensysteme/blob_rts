﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data.Values
{
    [CreateAssetMenu(fileName = "IntValue", menuName = "Data/Values/Int")]
    public class IntValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private int defaultValue;
        [NonSerialized] [ShowInInspector] private int runtimeValue;

        public event Action<int> ValueChanged;

        public int RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }
    }
}