﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data.Values
{
    [CreateAssetMenu(fileName = "IntValue", menuName = "Data/Values/Bool")]
    public class BoolValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private bool defaultValue;
        [NonSerialized] [ShowInInspector] private bool runtimeValue;

        public event Action<bool> ValueChanged;

        public bool RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }
    }
}