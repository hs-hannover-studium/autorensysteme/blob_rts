﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kniblings.Combat;
using Kniblings.Controllers;
using Kniblings.StateMachine;
using Kniblings.StateMachine.Unit;
using Kniblings.Utility;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(menuName = "Data/WaveGroup")]
    public class WaveGroupData : ScriptableObject
    {
        

        [Tooltip("Use Transform of a GameObject to set spawnPoint")]
        [SerializeField]
        private Transform spawnPointHelper;
        [Tooltip("Use Transform of a GameObject to set gatherPoint")]
        [SerializeField]
        private Transform gatherPointHelper;

        [SerializeField] private Vector3 spawnPoint;
        [SerializeField] private Vector3 gatherPoint;

        [SerializeField] private float randomDestinationRadius = 2f;
        [SerializeField] private List<UnitStruct> unitPrefabs;
        [SerializeField] [ReadOnly] private List<StateControllerUnit> units;

        public State unitStartState;
        
        [SerializeField, ReadOnly] private Color spawnPointColor = Color.yellow;
        [SerializeField, ReadOnly] private Color gatherPointColor = Color.green;

        public List<StateControllerUnit> Units => units;

        public Vector3 SpawnPoint => spawnPoint;

        public Vector3 GatherPoint => gatherPoint;

        public void SpawnGroup()
        {
            WaveController.Instance.SpawnFog(SpawnPoint);
            
            units.Clear();
            foreach (UnitStruct unitStruct in unitPrefabs)
            {
                for (int i = 0; i < unitStruct.count; i++)
                {
                    StateControllerUnit unit = Spawner.Spawn(unitStruct.controller, SpawnPoint, GatherPoint,
                        randomDestinationRadius);
                    units.Add(unit);
                    unit.currentState = unitStartState;
                    unit.Movement.MoveTo(unit.moveDestination);
                    WaveController.Instance.remainingEnemiesCount++;
                    unit.Damageable.Killed += OnKilled;
                }
            }
        }

        private void OnKilled(Damageable unit)
        {
            WaveController.Instance.ReduceRemainingEnemies();
            unit.Killed -= OnKilled;
        }

        [Button]
        public void ClearUnits()
        {
            units.Clear();
        }

        [Button]
        public void SetSpawnPoint()
        {
            spawnPoint = spawnPointHelper.transform.position;
        }

        [Button]
        public void SetGatherPoint()
        {
            gatherPoint = gatherPointHelper.transform.position;
        }

        [Button]
        public void ShowPoints()
        {
            Debug.DrawRay(spawnPoint, Vector3.up * 20, spawnPointColor, 3f);
            Debug.DrawRay(gatherPoint, Vector3.up * 20, gatherPointColor, 3f);
        }

        [Serializable]
        private struct UnitStruct
        {
            public StateControllerUnit controller;
            public int count;
        }
    }
}