﻿using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = "PlaceableBuildingsData", menuName = "Data/PlaceableBuildings", order = 0)]
    public class PlaceableBuildingsData : ScriptableObject
    {
        public BuildingData[] buildings;
    }
}