﻿using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = nameof(JobPrefabsData), menuName = "Data/JobPrefabs", order = 0)]
    public class JobPrefabsData : ScriptableObject
    {
        public GameObject magePrefab;
        public GameObject villagerPrefab;
        public GameObject warriorPrefab;
    }
}