﻿namespace Kniblings.Data.Enums
{
    // [System.Flags]
    public enum SelectionType
    {
        None,
        Single,
        Moveable
    }
}