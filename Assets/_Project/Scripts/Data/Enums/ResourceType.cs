﻿using System;

namespace Kniblings.Data.Enums
{
    /// <summary>
    ///     Defines all ressource types
    /// </summary>
    public enum ResourceType
    {
        Malisium,
        Wood,
        Stone,
    }

    /// <summary>
    ///     Class container for Serializable resource type values
    /// </summary>
    [Serializable]
    public struct ResourceTypeValue
    {
        public ResourceType type;
        public int value;

        public ResourceTypeValue(ResourceType type, int value)
        {
            this.type = type;
            this.value = value;
        }

        public ResourcePackage ToResourcePackage()
        {
            switch (type)
            {
                case ResourceType.Malisium:
                    return new ResourcePackage(value, 0,0);
                case ResourceType.Wood:
                    return new ResourcePackage(0, value, 0);
                case ResourceType.Stone:
                    return new ResourcePackage(0,0, value);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}