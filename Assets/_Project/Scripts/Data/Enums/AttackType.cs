﻿using System;

namespace Kniblings.Data.Enums
{
    /// <summary>
    ///     Defines all combat types
    /// </summary>
    public enum AttackType
    {
        None,
        Physical,
        Magical
    }

    /// <summary>
    ///     Class container for Serializable combat type values
    /// </summary>
    [Serializable]
    public struct AttackTypeValue
    {
        public AttackType type;
        public int value;
    }
}