﻿using Kniblings.Data.Enums;
using Kniblings.StateMachine;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = "Unit", menuName = "Data/Entities/Unit")]
    public class UnitData : EntityData
    {
        [VerticalGroup("Game Data/Stats")]
        [LabelWidth(100)]
        [Range(0.85f, 20f)]
        public float attackRange;

        [VerticalGroup("Game Data/Stats")]
        [LabelWidth(100)]
        [Tooltip("Attacks per second")]
        [Range(0.25f, 4f)]
        public float attackRate;

        [BoxGroup("Audio")] public AudioClip attackSound;

        [VerticalGroup("Game Data/Stats")]
        [LabelWidth(100)]
        public AttackType attackType;

        [VerticalGroup("Game Data/Stats")]
        [LabelWidth(100)]
        public int damage;

        [VerticalGroup("Game Data/Stats")]
        [LabelWidth(100)]
        public int defense;

        [BoxGroup("Audio")] public AudioClip dieSound;

        [VerticalGroup("Game Data/Stats")]
        [LabelWidth(100)]
        [Range(0f, 20f)]
        public float viewRange;

        [BoxGroup("Audio")] public AudioClip walkingSound;

        [BoxGroup("Locomotion")] public float walkSpeed = 3.5f;
        [BoxGroup("Locomotion")] public State defaultMoveState;
    }
}