﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = nameof(VillagerData), menuName = "Data/Entities/Villager", order = 0)]
    public class VillagerData : UnitData
    {
        [BoxGroup("Resources")] public int backpackCapacity;
    }
}