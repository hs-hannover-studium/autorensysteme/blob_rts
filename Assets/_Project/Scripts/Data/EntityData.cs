﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace Kniblings.Data
{
    public abstract class EntityData : ScriptableObject
    {
        [BoxGroup("Basic Info")]
        [SerializeField]
        [TextArea]
        private string description;

        [VerticalGroup("Game Data/Stats")]
        [SerializeField]
        [LabelWidth(100)]
        private int maxHealth;

        [BoxGroup("Basic Info")]
        [SerializeField]
        private new string name;

        [HorizontalGroup("Game Data", 75)]
        [PreviewField(75)]
        [HideLabel]
        [SerializeField]
        private GameObject prefab;
        
        [HorizontalGroup("Game Data", 75)]
        [PreviewField(75)]
        [HideLabel]
        [SerializeField]
        private Sprite icon;

        [FormerlySerializedAs("resourceList")]
        [FormerlySerializedAs("ressourceList")]
        [BoxGroup("Resources")]
        [SerializeField]
        private ResourcePackage resourceCost;

        public Sprite Icon => icon;

        public string Name => name;

        public string Description => description;

        public int MaxHealth => maxHealth;

        public GameObject Prefab => prefab;

        public ResourcePackage ResourceCost => resourceCost;
    }
}