﻿using System;

namespace Kniblings.Data
{
    [Serializable]
    public struct ResourcePackage
    {
        public int malisium;
        public int wood;
        public int stone;

        public ResourcePackage(int malisium, int wood, int stone)
        {
            this.malisium = malisium >= 0 ? malisium : 0;
            this.wood = wood >= 0 ? wood : 0;
            this.stone = stone >= 0 ? stone : 0;
        }

        // Overloads + operator
        public static ResourcePackage operator +(ResourcePackage a, ResourcePackage b)
        {
            return new ResourcePackage(
                a.malisium + b.malisium,
                a.wood + b.wood,
                a.stone + b.stone);
        }

        public int GetTotal()
        {
            return malisium + wood + stone;
        }
    }
}