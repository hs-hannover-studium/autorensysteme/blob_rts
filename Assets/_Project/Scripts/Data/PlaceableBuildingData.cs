﻿using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = nameof(PlaceableBuildingData), menuName = "Data/Entities/PlaceableBuilding",
        order = 0)]
    public class PlaceableBuildingData : BuildingData
    {
        public int capacity = 2;
        public JobType jobType = JobType.None;
    }


    public enum JobType
    {
        None,
        Warrior,
        Mage
    }
}