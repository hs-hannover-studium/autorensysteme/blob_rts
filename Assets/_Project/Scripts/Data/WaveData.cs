﻿using System.Collections.Generic;
using Kniblings.Controllers;
using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(menuName = "Data/Wave")]
    public class WaveData : ScriptableObject
    {
        [Tooltip("Time till next wave will be spawned")]
        public float duration;

        [Tooltip("Time after which units will attack WaveController target")]
        public float prepareTime;

        [SerializeField] private List<WaveGroupData> waveGroups;

        public List<WaveGroupData> WaveGroups => waveGroups;

        public void StartWave()
        {
            AudioController.Instance.PlayFxOneShot(AudioController.Instance.waveStarts);

            foreach (WaveGroupData g in WaveGroups)
            {
                g.SpawnGroup();
            }
        }
    }
}