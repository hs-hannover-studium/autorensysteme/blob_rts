﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = "RuntimeResourceData", menuName = "Data/RuntimeResource", order = 0)]
    public class RuntimeResourceData : ScriptableObject, ISerializationCallbackReceiver
    {
        private int _malisium;
        private int _stone;
        private int _wood;
        [SerializeField] private ResourcePackage defaultValues;
        [SerializeField] private int maximumValue = 999;


        [ShowInInspector]
        [ShowInInlineEditors]
        [ShowIf("@UnityEngine.Application.isPlaying")]
        public ResourcePackage RuntimeValues
        {
            get => new ResourcePackage(_malisium, _wood, _stone);
            private set
            {
                _malisium = value.malisium;
                _wood = value.wood;
                _stone = value.stone;
                RuntimeValuesChanged?.Invoke(RuntimeValues);
            }
        }

        public void OnBeforeSerialize()
        {
            //ignore
        }

        public void OnAfterDeserialize()
        {
            SetDefaultValues();
        }

        public event Action<ResourcePackage> RuntimeValuesChanged;
        public static event Action<int> MalisiumAdded;
        public static event Action<int> StoneAdded;
        public static event Action<int> WoodAdded;

        public void SetDefaultValues()
        {
            RuntimeValues = defaultValues;
        }

        public void AddResources(ResourcePackage package)
        {
            _malisium += Mathf.Min(package.malisium, maximumValue - _malisium);
            if (package.malisium > 0)
                MalisiumAdded?.Invoke(package.malisium);
            
            _wood += Mathf.Min(package.wood, maximumValue - _wood);
            if (package.wood > 0)
                WoodAdded?.Invoke(package.wood);
            
            _stone += Mathf.Min(package.stone, maximumValue - _stone);
            if (package.stone > 0)
                StoneAdded?.Invoke(package.stone);
            
            RuntimeValuesChanged?.Invoke(RuntimeValues);
        }

        public void RemoveResources(ResourcePackage package)
        {
            _malisium -= Mathf.Min(package.malisium, _malisium);
            _wood -= Mathf.Min(package.wood, _wood);
            _stone -= Mathf.Min(package.stone, _stone);
            RuntimeValuesChanged?.Invoke(RuntimeValues);
        }
    }
}