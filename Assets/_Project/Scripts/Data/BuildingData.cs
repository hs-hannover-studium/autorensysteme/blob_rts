﻿using UnityEngine;

namespace Kniblings.Data
{
    [CreateAssetMenu(fileName = nameof(BuildingData), menuName = "Data/Entities/Building", order = 0)]
    public class BuildingData : EntityData
    {
        [SerializeField] public BuildingType type;
    }

    public enum BuildingType
    {
        None,
        VillageCore,
        Placeable
    }
}