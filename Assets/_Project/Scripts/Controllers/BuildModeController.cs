﻿using Kniblings.CustomInput;
using Kniblings.Data;
using Kniblings.Selection;
using Kniblings.World.BuildMode;
using UnityEngine;

namespace Kniblings.Controllers
{
    [RequireComponent(typeof(BuildModePlacement))]
    public class BuildModeController : MonoBehaviour
    {
        private bool _active;
        private BuildModePlacement _placement;

        private void Awake()
        {
            _placement = GetComponent<BuildModePlacement>();
        }

        private void OnEnable()
        {
            InputController.Actions.toggleBuildMode.Down += ToggleMode;
            BuildModeCaller.BuildModeToggle += ToggleMode;
        }

        private void OnDisable()
        {
            InputController.Actions.toggleBuildMode.Down -= ToggleMode;
            BuildModeCaller.BuildModeToggle -= ToggleMode;
        }

        public void ToggleMode()
        {
            if (GameController.Instance.gamePaused || GameController.Instance.gameWon ||
                GameController.Instance.gameLost)
                return;

            if (_active)
                LeaveMode();
            else
                EnterMode();
        }

        public void EnterMode()
        {
            GameController.Instance.GamePaused += LeaveMode;
            GameController.Instance.GameWon += LeaveMode;
            GameController.Instance.GameLost += LeaveMode;

            BuildModeCaller.ModeEnteredCall();
            BuildModeCaller.BuilderRequest += BuildModeCallerOnBuilderRequest;
            AudioController.Instance.PlayFxOneShot(AudioController.Instance.uiWindowOpen);
            _active = true;
            SelectedObjects.SelectionChanged += LeaveMode;
        }

        public void LeaveMode()
        {
            GameController.Instance.GamePaused -= LeaveMode;
            GameController.Instance.GameWon -= LeaveMode;
            GameController.Instance.GameLost -= LeaveMode;
            SelectedObjects.SelectionChanged -= LeaveMode;
            
            _placement.AbortPlacement();
            BuildModeCaller.ModeLeftCall();
            BuildModeCaller.BuilderRequest -= BuildModeCallerOnBuilderRequest;
            AudioController.Instance.PlayFxOneShot(AudioController.Instance.uiWindowClose);
            _active = false;
        }

        public void LeaveMode(int value)
        {
            LeaveMode();
        }

        private void BuildModeCallerOnBuilderRequest(BuildingData data)
        {
            if (PlayerController.Instance.CompareResources(data.ResourceCost))
            {
                _placement.AbortPlacement();
                _placement.StartPlacement(data, OnSuccess);
            }
        }

        private void OnSuccess()
        {
            //nothing
        }
    }
}