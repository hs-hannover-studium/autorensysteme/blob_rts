﻿using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.Locomotion;
using Kniblings.Selection;
using UnityEngine;

namespace Kniblings.Controllers
{
    public class MovementController : MonoBehaviour
    {
        private bool _isDragging;
        private Vector3 _p1;
        [SerializeField] private Camera cam;
        [SerializeField] private Formation formation;
        [SerializeField] private LayerMask groundLayer;

        private void Update()
        {
            if (Input.GetMouseButtonDown(1)) _p1 = Input.mousePosition;

            if (Input.GetMouseButton(1)) _isDragging = (_p1 - Input.mousePosition).magnitude > 20;

            if (Input.GetMouseButtonUp(1) && !_isDragging) MoveCommand();
        }

        private void MoveCommand()
        {
            if (SelectedObjects.SelectionType != SelectionType.Moveable)
                return;

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, groundLayer)) return;

            formation.Move(hit);
        }
    }
}