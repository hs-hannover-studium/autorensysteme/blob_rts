﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Kniblings.Combat;
using Kniblings.Data;
using Kniblings.StateMachine;
using Kniblings.StateMachine.Unit;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Action = System.Action;
#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;

#endif

namespace Kniblings.Controllers
{
    /// <summary>
    ///     Responsible for spawning enemy waves.
    /// </summary>
    [DefaultExecutionOrder(-6)]
    public class WaveController : MonoBehaviour
    {
        public static WaveController Instance { get; private set; }

        public GameObject fogPrefab;
        public State chaseState;
        public int initialWaitSeconds = 5;
        public bool isActive = true;
        public List<WaveData> waves;
        [ReadOnly] public int remainingEnemiesCount;

        [ReadOnly] [SerializeField] private WaveData currentWaveData;
        [SerializeField] private Damageable target;
        private bool _wavesCompleted;
        private int _currentWaveIndex = 1;

        public int CurrentWaveIndex => _currentWaveIndex;
        public WaveData CurrentWaveData => currentWaveData;

        public event Action WavesStarted;
        public event Action WaveStarted;
        public event Action WaveCompleted;
        public event Action WavesCompleted;

        public event Action EnemyKilled;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            if (!isActive)
                return;

            StartCoroutine(InitialWaitTime());
        }

        private IEnumerator InitialWaitTime()
        {
            yield return new WaitForSeconds(initialWaitSeconds);
            StartWaves();
        }

        // TODO: Call from GameController to start Waves
        [Button]
        public void StartWaves()
        {
            isActive = true;
            StartCoroutine(SpawnWaves());
        }

        protected IEnumerator SpawnWaves()
        {
            WavesStarted?.Invoke();

            foreach (WaveData wave in waves)
            {
                currentWaveData = wave;
                WaveStarted?.Invoke();

                wave.StartWave();

                yield return new WaitForSeconds(wave.prepareTime);

                // Sends all Units to their targets
                foreach (WaveGroupData waveGroup in wave.WaveGroups)
                {
                    foreach (StateControllerUnit unit in waveGroup.Units)
                    {
                        unit.absoluteTarget = target;
                        unit.currentTarget = target;
                        unit.moveDestination = target.transform.position;
                        unit.TransitionToState(chaseState);
                    }
                }

                _currentWaveIndex++;
                WaveCompleted?.Invoke();
                yield return new WaitForSeconds(wave.duration);
            }

            _wavesCompleted = true;
        }

        public void SpawnFog(Vector3 position)
        {
            GameObject fog = Instantiate(fogPrefab, position, Quaternion.identity);

            StartCoroutine(HideFog(fog, 30, position));
        }

        private IEnumerator HideFog(GameObject fog, int seconds, Vector3 position)
        {
            yield return new WaitForSeconds(seconds);
            fog.transform.DOMove(position - (Vector3.up * 10f), 15f).SetEase(Ease.InSine).OnComplete(() =>
            {
                Destroy(fog);
            });
        }

        public void ReduceRemainingEnemies()
        {
            remainingEnemiesCount--;
            EnemyKilled?.Invoke();

            if (remainingEnemiesCount <= 0 && (_wavesCompleted || _currentWaveIndex - 1 == waves.Count))
                WavesCompleted?.Invoke();
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(WaveController), true)]
    public class WaveControllerEditor : OdinEditor
    {
        public void OnSceneGUI()
        {
            //base.OnInspectorGUI();
            WaveController t = (WaveController) target;

            foreach (WaveData wave in t.waves)
                for (int i = 0; i < wave.WaveGroups.Count; i++)
                {
                    Handles.Label(wave.WaveGroups[i].SpawnPoint + Vector3.up * 15, "WaveGroup: " + i);

                    // SpawnPoint
                    Handles.Label(wave.WaveGroups[i].SpawnPoint + Vector3.up * 11, "Spawn");
                    Handles.color = Color.white;
                    Handles.DrawLine(wave.WaveGroups[i].SpawnPoint, wave.WaveGroups[i].SpawnPoint + Vector3.up * 10);

                    // GatherPoint
                    Handles.Label(wave.WaveGroups[i].GatherPoint + Vector3.up * 11, "Gather");
                    Handles.color = Color.blue;
                    Handles.DrawLine(wave.WaveGroups[i].GatherPoint, wave.WaveGroups[i].GatherPoint + Vector3.up * 10);
                }
        }
    }
#endif
}