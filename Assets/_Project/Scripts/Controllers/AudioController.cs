﻿using Kniblings.Data;
using Kniblings.Data.Values;
using Kniblings.Utility;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

namespace Kniblings.Controllers
{
    [DefaultExecutionOrder(-10)]
    public class AudioController : MonoBehaviour
    {
        [Required] public AudioClip audioClipMainTheme;
        [Required] public AudioSource audioSourceFX;

        [Required] public AudioSource audioSourceMusic;
        [FoldoutGroup("Building")] [Required] public AudioClip buildingDeath;
        [FoldoutGroup("Building")] [Required] public AudioClip buildingDemolish;

        [FoldoutGroup("Building")] [Required] public AudioClip buildingPlaced;
        // [FoldoutGroup("Building")] [Required] public AudioClip buildingTakeDamage;

        [FoldoutGroup("Audio Volumes")]
        [SerializeField]
        public FloatValue fxVolume;

        [FoldoutGroup("GamePlay")] [Required] public AudioClip gamePlayGameLost;

        [FoldoutGroup("GamePlay")] [Required] public AudioClip gamePlayGameWon;

        [FoldoutGroup("Audio Volumes")]
        [SerializeField]
        public FloatValue masterVolume;

        [Required] public AudioMixer mixer;

        [FoldoutGroup("Audio Volumes")]
        [SerializeField]
        public FloatValue musicVolume;
        // [FoldoutGroup("Unit")] [Required] public AudioClip unitSpawn;

        [FoldoutGroup("Projectile")]
        [Required]
        public AudioClip projectileFireballExplode;

        [FoldoutGroup("UI")] [Required] public AudioClip uiBtnClick;
        [FoldoutGroup("UI")] [Required] public AudioClip uiBtnHover;
        [FoldoutGroup("UI")] [Required] public AudioClip uiWindowClose;
        [FoldoutGroup("UI")] [Required] public AudioClip uiWindowOpen;
        // [FoldoutGroup("Unit")] [Required] public AudioClip unitDeath;
        [FoldoutGroup("Unit")] [Required] public AudioClip unitMeleeAttack;

        [FoldoutGroup("Unit")] [Required] public AudioClip unitMovement;

        // [FoldoutGroup("Unit")] [Required] public AudioClip unitTakeDamage;

        [FoldoutGroup("Wave")] [Required] public AudioClip waveStarts;
        public static AudioController Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
            masterVolume.RuntimeValue = 0f;
            musicVolume.RuntimeValue = 0f;
            fxVolume.RuntimeValue = 0f;

        }

        public void PlayFxOneShot(AudioClip clip)
        {
            audioSourceFX.PlayOneShot(clip);
        }

        public void SetVolume(float value, AudioChannel channel)
        {
            switch (channel)
            {
                case AudioChannel.Master:
                    mixer.SetFloat("MasterVolume", value);
                    masterVolume.RuntimeValue = value;
                    break;
                case AudioChannel.Music:
                    mixer.SetFloat("MusicVolume", value);
                    musicVolume.RuntimeValue = value;
                    break;
                case AudioChannel.Fx:
                    mixer.SetFloat("FxVolume", value);
                    fxVolume.RuntimeValue = value;
                    break;
            }

        }
    }

    public enum AudioChannel
    {
        Master,
        Music,
        Fx
    }
}