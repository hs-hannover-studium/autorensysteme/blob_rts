﻿using System;
using Cinemachine;
using Kniblings.CustomInput;
using Kniblings.Utility;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace Kniblings.Controllers
{
    public class CameraController : MonoBehaviour
    {
        #region members


        [FoldoutGroup("Movement", true)] public float moveSpeedAtMinZoom;
        [FoldoutGroup("Movement", true)] public float moveSpeedAtMaxZoom;
        [FoldoutGroup("Movement", true)] public float zoomSpeedPower = 4f;
        [FoldoutGroup("Movement", true)] public float movementTime = 5f;
        [FoldoutGroup("Movement", true)] public float dragRotationScale = 6f;
        [FoldoutGroup("Movement", true)] public float rotationScale = 3f;
        [FoldoutGroup("Movement", true)] public Vector2 horizontalAxisClamp;
        [FoldoutGroup("Movement", true)] public Collider boundingVolume;
        [FoldoutGroup("Movement", true), Range(0f, 0.5f)]
        public float edgeScrollArea = 0.1f;


        [FoldoutGroup("Zoom", true)] public float zoomSpeed = 0.3f;
        [FoldoutGroup("Zoom", true)] public float minimumDistance = 2f;
        [FoldoutGroup("Zoom", true)] public float maximumDistance = 30f;
        [FoldoutGroup("Zoom", true)] public float heightAdjustFactor = 0.05f;

        [HorizontalGroup] public bool drawDebug;
        [ShowInInspector, HorizontalGroup]
        public bool LockCursor
        {
            get => Cursor.lockState == CursorLockMode.Confined;
            set => Cursor.lockState = value ? CursorLockMode.Confined : CursorLockMode.None;
        }

        private Transform _virtualCamera;
        private Transform _verticalAxisTransform;
        private Transform _horizontalAxisTransform;
        
        private Vector3 _newPos;
        private Quaternion _newRotVerticalAxis;
        private Quaternion _newRotHorizontalAxis;
        private float _zoomDistance;
        private Vector3 _rotationDragLast;
        private Vector3 _rotationDragCurrent;
        private bool _rotationDrag;
        private float _realMoveSpeed;

        #endregion

        private void Awake()
        {
            _verticalAxisTransform = transform.GetChild(0);
            _horizontalAxisTransform = _verticalAxisTransform.GetChild(0);
            _virtualCamera = _horizontalAxisTransform.GetChild(0);
        }

        private void Start()
        {
            _newPos = _verticalAxisTransform.position;
            _zoomDistance = _virtualCamera.localPosition.z;
            _newRotVerticalAxis = _verticalAxisTransform.localRotation;
            _newRotHorizontalAxis = _horizontalAxisTransform.localRotation;
        }

        private void OnEnable()
        {
            InputController.Actions.rightMouse.Down += RightMouseOnDown;
            InputController.Actions.rightMouse.Up += RightMouseOnUp;
            InputController.Actions.lockCursor.Down += LockCursorOnDown;
            InputController.Actions.focusOnVillageCore.Down += FocusOnVillageCoreOnDown;
        }



        private void OnDisable()
        {
            InputController.Actions.rightMouse.Down -= RightMouseOnDown;
            InputController.Actions.rightMouse.Up -= RightMouseOnUp;
            InputController.Actions.lockCursor.Down -= LockCursorOnDown;
            InputController.Actions.focusOnVillageCore.Down -= FocusOnVillageCoreOnDown;
        }

        private void OnValidate()
        {
            minimumDistance = Mathf.Clamp(minimumDistance, 0f, maximumDistance);
            maximumDistance = Mathf.Clamp(maximumDistance, minimumDistance, 1000f);
        }


        private void LockCursorOnDown()
        {
            LockCursor = !LockCursor;
        }

        private void FocusOnVillageCoreOnDown()
        {
            _newPos = PlayerController.Instance.villageCore.position;
        }
        
        private void FixedUpdate()
        {
            _realMoveSpeed = CalculateRealMoveSpeed();
            HandleMouseInput();
            HandleKeyboardInput();
            
            ApplyZoom();
            ApplyMovement();
            ApplyRotation();
        }

        private void RightMouseOnUp()
        {
            _rotationDrag = false;
        }

        private void RightMouseOnDown()
        {
            Vector3 mousePosition = InputController.GetMousePosition();
            _rotationDragLast = new Vector3(mousePosition.x / Screen.width, mousePosition.y / Screen.height, 0);
            _rotationDrag = true;
        }

        private void HandleKeyboardInput()
        {
            Vector3 oldPos = _newPos;
            // Movement
            if (InputController.Actions.cameraForward.IsPressed) _newPos += _verticalAxisTransform.forward * -_realMoveSpeed;

            if (InputController.Actions.cameraBackwards.IsPressed) _newPos += _verticalAxisTransform.forward * _realMoveSpeed;

            if (InputController.Actions.cameraRight.IsPressed) _newPos += _verticalAxisTransform.right * -_realMoveSpeed;

            if (InputController.Actions.cameraLeft.IsPressed) _newPos += _verticalAxisTransform.right * _realMoveSpeed;

            if (!PositionInBounds(_newPos))
                _newPos = oldPos;

            // Zooming
            if (InputController.Actions.cameraZoomIn.IsPressed)
                _zoomDistance += zoomSpeed * 0.2f;
            else if (InputController.Actions.cameraZoomOut.IsPressed)
                _zoomDistance -= zoomSpeed * 0.2f;
            
            // Rotation
            if (InputController.Actions.cameraRotateRight.IsPressed)
                _newRotVerticalAxis *= Quaternion.Euler(Vector3.up * -rotationScale);
            
            else if (InputController.Actions.cameraRotateLeft.IsPressed)
                _newRotVerticalAxis *= Quaternion.Euler(Vector3.up * rotationScale);
        }


        private void HandleMouseInput()
        {
            _zoomDistance += InputController.GetMouseScrollDelta() * -zoomSpeed;

            if (LockCursor)
                ScreenEdgeScrolling();

            if (!_rotationDrag) return;

            // Get mouse position and normalize it
            Vector3 mousePosition = InputController.GetMousePosition();
            _rotationDragCurrent = new Vector3(mousePosition.x / Screen.width, mousePosition.y / Screen.height, 0);
            
            // Difference in mouse position between last frame and current frame
            Vector3 diff = _rotationDragLast - _rotationDragCurrent;
            _rotationDragLast = _rotationDragCurrent;
            
            // Update rotation for the 2 gimbals
            _newRotVerticalAxis *= Quaternion.Euler(new Vector3(
                0,
                -diff.x * dragRotationScale,
                0));
            
            _newRotHorizontalAxis *= Quaternion.Euler(new Vector3(
                -diff.y * dragRotationScale,
                0,
                0));
        }
        
        private void ScreenEdgeScrolling()
        {
            //block edge scrolling while rotating
            if (_rotationDrag)
                return;

            Vector3 mousePosAbsolute = InputController.GetMousePosition();
            Vector3 mousePosRelative =
                new Vector3(mousePosAbsolute.x / Screen.width, mousePosAbsolute.y / Screen.height);

            Vector3 oldPos = _newPos;
            if (mousePosRelative.x < edgeScrollArea) // left
                _newPos += _verticalAxisTransform.right * _realMoveSpeed;
            if (mousePosRelative.x > 1f - edgeScrollArea) // right
                _newPos += _verticalAxisTransform.right * -_realMoveSpeed;
            if (mousePosRelative.y < edgeScrollArea) // bottom
                _newPos += _verticalAxisTransform.forward * _realMoveSpeed;
            if (mousePosRelative.y > 1f - edgeScrollArea) // top
                _newPos += _verticalAxisTransform.forward * -_realMoveSpeed;

            if (!PositionInBounds(_newPos))
                _newPos = oldPos;
        }

        // Modulates the moveSpeed with the zoomDistance so that it is faster with more distance
        private float CalculateRealMoveSpeed()
        {
            return moveSpeedAtMinZoom + Mathf.Pow(_zoomDistance / maximumDistance, zoomSpeedPower) * (moveSpeedAtMaxZoom - moveSpeedAtMinZoom);
        }

        private void ApplyMovement()
        {
            _verticalAxisTransform.position = Vector3.Lerp(_verticalAxisTransform.position, _newPos, Time.deltaTime * movementTime);
            // _verticalAxisTransform.position = Vector3.Lerp(_verticalAxisTransform.position, _newPos, 0.1f);
        }

        private void ApplyRotation()
        {
            _verticalAxisTransform.localRotation =
                Quaternion.Lerp(_verticalAxisTransform.localRotation, _newRotVerticalAxis, Time.deltaTime * movementTime);

            // Clamp rotation around x between the defined values in horizontalAxisClamp
            Vector3 h = _newRotHorizontalAxis.eulerAngles;
            h = new Vector3(Mathf.Clamp(h.x, horizontalAxisClamp.x, horizontalAxisClamp.y), h.y, h.z);
            _newRotHorizontalAxis = Quaternion.Euler(h);

            _horizontalAxisTransform.localRotation =
                Quaternion.Lerp(_horizontalAxisTransform.localRotation, _newRotHorizontalAxis, Time.deltaTime * movementTime);
        }
        
        private void ApplyZoom()
        {
            _zoomDistance = Mathf.Clamp(_zoomDistance, minimumDistance, maximumDistance);
            
            // Camera gets moved on the z Axis only -> distance to focus point
            _virtualCamera.localPosition = Vector3.Lerp(_virtualCamera.localPosition, new Vector3(0, 0, _zoomDistance), Time.deltaTime * movementTime);
            
            _horizontalAxisTransform.localPosition = Vector3.Lerp(_horizontalAxisTransform.localPosition, new Vector3(0, _zoomDistance * heightAdjustFactor, 0), Time.deltaTime * movementTime );
        }
        
        private bool PositionInBounds(Vector3 position)
        {
            return boundingVolume.bounds.Contains(position);
        }


        private void OnDrawGizmos()
        {
            if (!drawDebug)
                return;

            Awake();
            
            // Vertical Axis Gimbal
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(_verticalAxisTransform.position, 2f);

            // Horizontal Axis Gimbal with triangle
            Gizmos.color = Color.yellow;
            Vector3 vCamPos = _virtualCamera.position;
            Vector3 gimbalPos = _horizontalAxisTransform.position;
            Gizmos.DrawWireSphere(gimbalPos, 1.5f);
            Gizmos.DrawLine(vCamPos, gimbalPos);
            Gizmos.DrawLine(vCamPos, new Vector3(vCamPos.x, gimbalPos.y, vCamPos.z));
            Gizmos.DrawLine(new Vector3(vCamPos.x, gimbalPos.y, vCamPos.z), gimbalPos);
        }
    }
}