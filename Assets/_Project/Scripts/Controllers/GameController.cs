﻿using System;
using Kniblings.Combat;
using Kniblings.CustomInput;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Controllers
{
    [DefaultExecutionOrder(-5)]
    public class GameController : MonoBehaviour
    {
        [Required] public Damageable villageCore;
        [ReadOnly] public bool gameLost;

        [ReadOnly] public bool gamePaused;
        [ReadOnly] public bool gameWon;
        public static GameController Instance { get; private set; }

        public event Action GamePaused;
        public event Action GameResumed;
        public event Action GameWon;
        public event Action GameLost;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
        }

        private void Start()
        {
            Time.timeScale = 1;
        }

        private void OnEnable()
        {
            InputController.Actions.togglePauseMenu.Down += TogglePauseMenu;
            WaveController.Instance.WavesCompleted += OnGameWon;
            villageCore.Killed += OnGameLost;
        }

        private void OnDisable()
        {
            InputController.Actions.togglePauseMenu.Down -= TogglePauseMenu;
            WaveController.Instance.WavesCompleted -= OnGameWon;
            villageCore.Killed -= OnGameLost;
        }

        [Button]
        public void OnGameWon()
        {
            GameWon?.Invoke();
            Time.timeScale = 0;
            gameWon = true;
        }

        [Button]
        public void OnGameLost()
        {
            GameLost?.Invoke();
            Time.timeScale = 0;
            gameLost = true;
        }

        [Button]
        public void PauseGame()
        {
            gamePaused = true;
            Time.timeScale = 0;
            GamePaused?.Invoke();
        }

        [Button]
        public void ResumeGame()
        {
            gamePaused = false;
            Time.timeScale = 1;
            GameResumed?.Invoke();
        }
        
        private void OnGameLost(Damageable damageable)
        {
            GameLost?.Invoke();
            Time.timeScale = 0;
            gameLost = true;
        }

        public void TogglePauseMenu()
        {
            if (gameWon || gameLost)
                return;

            if (gamePaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }
}