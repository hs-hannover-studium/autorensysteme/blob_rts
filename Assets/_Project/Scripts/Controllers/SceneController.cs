﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Kniblings.Controllers
{
    public class SceneController : MonoBehaviour
    {
        private static SceneController _instance;

        [Obsolete("Use SceneManager.sceneLoaded instead")]
        public static event Action SceneLoadComplete;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        public static void LoadScene(int index)
        {
            AsyncOperation ao = SceneManager.LoadSceneAsync(index);
            ao.completed += OnOperationCompleted;
        }

        private static void OnOperationCompleted(AsyncOperation ao)
        {
            SceneLoadComplete?.Invoke();
            ao.completed -= OnOperationCompleted;
        }
    }
}