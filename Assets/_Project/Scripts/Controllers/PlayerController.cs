﻿using System;
using System.Collections.Generic;
using Kniblings.Data;
using Kniblings.Data.Values;
using Kniblings.Other;
using Kniblings.World.Buildings;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Controllers
{
    [DefaultExecutionOrder(-1)]
    public class PlayerController : MonoBehaviour
    {
        public KniblingsValue kniblings;
        public int kniblingsLimit = 40;

        [InlineEditor] public RuntimeResourceData resourceData;

        // All kniblings assigned to the player
        [ReadOnly] public List<PlaceableBuilding> buildings = new List<PlaceableBuilding>();
        
        public Transform villageCore;

        public event Action<ResourcePackage> ResourceGenerated; 
        
        public ResourcePackage resourcePerTick;
        public float tickRate = 1f;

        private Coroutine _malisiumCoroutine;
        
        #region Singleton

        public static PlayerController Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
        }

        #endregion
        
        public event Action kniblingsLimitReached; 

        private void Start()
        {
            resourceData.SetDefaultValues();
            StartPassiveMalisium();
        }

        private void OnEnable()
        {
            GameController.Instance.GameResumed += StartPassiveMalisium;
        }

        private void OnDisable()
        {
            GameController.Instance.GameResumed -= StartPassiveMalisium;
        }

        private void StartPassiveMalisium()
        {
            if (_malisiumCoroutine == null)
                _malisiumCoroutine = StartCoroutine(PassiveMalisiumCoroutine());
        }

        public bool SpendResources(ResourcePackage package)
        {
            if (!CompareResources(package))
                return false;

            resourceData.RemoveResources(package);
            return true;
        }

        /// <summary>
        ///     Check if the Player has the resources, without changing the values
        /// </summary>
        /// <param name="costPackage">Resources to be spent</param>
        /// <returns>true if enough resources, false if not</returns>
        public bool CompareResources(ResourcePackage costPackage)
        {
            return costPackage.malisium <= resourceData.RuntimeValues.malisium
                   && costPackage.stone <= resourceData.RuntimeValues.stone
                   && costPackage.wood <= resourceData.RuntimeValues.wood;
        }

        public void AddResources(ResourcePackage package)
        {
            resourceData.AddResources(package);
        }

        [Button]
        public void ClearUnits()
        {
            kniblings.RuntimeValue.Clear();
        }
        
        private IEnumerator PassiveMalisiumCoroutine()
        {
            while (!GameController.Instance.gamePaused)
            {
                yield return new WaitForSeconds(1 / tickRate);
                AddResources(resourcePerTick);
                ResourceGenerated?.Invoke(resourcePerTick);
            }

            _malisiumCoroutine = null;
        }
        
        public void OnKniblingsLimitReached()
        {
            Debug.Log("Limit Reached");
            kniblingsLimitReached?.Invoke();
        }
    }
}