using System.Collections.Generic;
using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.Other;
using Kniblings.Selection;
using Kniblings.Utility.Draw;
using Kniblings.World.BuildMode;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Kniblings.Controllers
{
    public class SelectionController : MonoBehaviour
    {
        private void OnEnable()
        {
            GameController.Instance.GamePaused += SelectedObjects.DeselectAll;
            GameController.Instance.GamePaused += DisableSelection;
            GameController.Instance.GameResumed += EnableSelection;
            GameController.Instance.GameWon += DisableSelection;
            GameController.Instance.GameWon += SelectedObjects.DeselectAll;
            GameController.Instance.GameLost += DisableSelection;
            GameController.Instance.GameLost += SelectedObjects.DeselectAll;
            BuildModeCaller.PlacementModeEntered += DisableSelection;
            BuildModeCaller.PlacementModeLeft += EnableSelection;
            BuildModeCaller.BuildModeEntered += SelectedObjects.DeselectAll;
        }

        private void OnDisable()
        {
            GameController.Instance.GamePaused -= SelectedObjects.DeselectAll;
            GameController.Instance.GamePaused -= DisableSelection;
            GameController.Instance.GameResumed -= EnableSelection;
            GameController.Instance.GameWon -= DisableSelection;
            GameController.Instance.GameWon -= SelectedObjects.DeselectAll;
            GameController.Instance.GameLost -= DisableSelection;
            BuildModeCaller.PlacementModeEntered -= DisableSelection;
            BuildModeCaller.PlacementModeLeft -= EnableSelection;
            GameController.Instance.GameLost -= SelectedObjects.DeselectAll;
            
            BuildModeCaller.BuildModeEntered -= SelectedObjects.DeselectAll;
        }

        private void Update()
        {
            //TODO: Disable unit seleciton in build mode and enable both in "normal mode"
            //TODO: OnBuildModePlacement (Dragging a Building) disable selection for everything
            if (!allowSelection || GameController.Instance.gamePaused)
                return;

            if (Input.GetMouseButtonDown(0)) _p1 = Input.mousePosition;

            if (Input.GetMouseButton(0)) _dragSelect = (_p1 - Input.mousePosition).magnitude > 40;

            if (Input.GetMouseButtonUp(0))
            {
                if (_dragSelect == false)
                    ClickSelect();
                else
                    DragSelect();

                // Debug.Log("Selected: " + SelectedObjects.Selected.Count);

                _dragSelect = false;
            }
        }

        private void OnGUI()
        {
            if (!_dragSelect || GameController.Instance.gamePaused)
                return;

            Rect rect = DrawRect.GetScreenRect(_p1, Input.mousePosition);
            DrawRect.DrawScreenRect(rect, selectionBoxColor);
            DrawRect.DrawScreenRectBorder(rect, 2, selectionBoxBorderColor);
        }

        /// <summary>
        ///     Checks if given transformation position is within Viewport bounds of selection rect
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool IsWithinSelectionBounds(Transform t)
        {
            Bounds viewportBounds = DrawRect.GetViewportBounds(cam, _p1, _p2);
            return viewportBounds.Contains(cam.WorldToViewportPoint(t.position));
        }

        #region Variables

        public bool allowSelection = true;
        [SerializeField] private Color selectionBoxColor;
        [SerializeField] private Color selectionBoxBorderColor;
        [SerializeField] private LayerMask selectionLayer;
        [SerializeField] private LayerMask uiLayer;
        [SerializeField] private Camera cam;

        // Mouse positions when dragging
        private Vector3 _p1;
        private Vector3 _p2;

        private bool _dragSelect;

        #endregion

        #region Selection Methods

        public void DisableSelection()
        {
            allowSelection = false;
        }

        public void EnableSelection()
        {
            allowSelection = true;
        }

        /// <summary>
        ///     Handles single click selection
        /// </summary>
        private void ClickSelect()
        {
            // -> Is Pointer over UI? Takes all Event System things, possible errors later
            if (EventSystem.current.IsPointerOverGameObject()) return;

            Ray ray = cam.ScreenPointToRay(_p1);
            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, selectionLayer))
            {
                SelectedObjects.DeselectAll();
                return;
            }

            GameObject o = hit.transform.gameObject;
            Selectable s = o.GetComponent<Selectable>();

            if (!s.allowSelection)
                return;

            // Deselect everything if "object to be selected" is of a different selection type
            if (s.Type != SelectedObjects.SelectionType || s.Type == SelectionType.Single)
                SelectedObjects.DeselectAll();

            if (Input.GetKey(KeyCode.LeftShift)) // inclusive select
            {
                if (SelectedObjects.Selected.Contains(s)) // deselect already selected
                    SelectedObjects.Deselect(s);
                else
                    SelectedObjects.Add(s);
            }
            else // exclusive select
            {
                SelectedObjects.DeselectAll();
                SelectedObjects.Add(s);
            }
        }

        /// <summary>
        ///     Handles drag selection
        /// </summary>
        private void DragSelect()
        {
            _p2 = Input.mousePosition;

            if (!Input.GetKey(KeyCode.LeftShift)) SelectedObjects.DeselectAll();

            List<Selectable> selectables = new List<Selectable>();
            foreach (Knibling unit in PlayerController.Instance.kniblings.RuntimeValue)
            {
                if (!unit.StateController.TryGetComponent(out Selectable s) || !s.allowSelection)
                    continue;

                if (!IsWithinSelectionBounds(unit.GetTransform()))
                    continue;
                selectables.Add(s);
            }

            SelectedObjects.Add(selectables);
        }

        #endregion
    }
}