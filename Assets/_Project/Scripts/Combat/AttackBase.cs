﻿using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.StateMachine.Unit;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Kniblings.Combat
{
    public abstract class AttackBase : MonoBehaviour
    {
        private readonly int _attackAnimID = Animator.StringToHash("Attack");
        [Required] public StateControllerUnit controller;

        public float NextAttackTime { get; set; }
        public float AttackRate { get; private set; }
        public float AttackRange { get; private set; }
        public AttackType AttackType { get; private set; }

        protected virtual void Start()
        {
            controller = controller ? controller : GetComponent<StateControllerUnit>();
        }

        protected virtual void Awake()
        {
            AttackRate = controller.UnitData.attackRate;
            AttackRange = controller.UnitData.attackRange;
            AttackType = controller.UnitData.attackType;
        }

        [Button]
        public void Attack()
        {
            controller.Movement.NavMeshAgent.isStopped = true;
            controller.Animator.SetTrigger(_attackAnimID);
        }

        /// <summary>
        /// </summary>
        /// <param name="target"></param>
        public virtual void Attack(Damageable target)
        {
            controller.Movement.NavMeshAgent.isStopped = true;

            controller.Movement.FaceTarget(target.transform);
            controller.Animator.SetTrigger(_attackAnimID);

            controller.AttackBase.NextAttackTime = Time.time + 1f / controller.UnitData.attackRate;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(AttackBase), true)]
    public class AttackBaseEditor : Editor
    {
        public void OnSceneGUI()
        {
            AttackBase attackBase = (AttackBase) target;
            Transform t = attackBase.transform;
            // Attack range
            Handles.color = Color.white;
            Handles.DrawWireArc(t.position, Vector3.up, Vector3.forward, 360,
                attackBase.controller.UnitData.attackRange);
            // Look direction
            Handles.color = Color.magenta;
            Handles.DrawLine(t.position, t.position + t.forward * attackBase.controller.UnitData.viewRange);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            AttackBase t = target as AttackBase;

            if (GUILayout.Button("Attack"))
                t.Attack();
        }
    }
#endif
}