﻿using System;
using Kniblings.Controllers;
using Random = UnityEngine.Random;

namespace Kniblings.Combat
{
    public class MeleeAttack : AttackBase
    {
        public int Damage { get; protected set; }

        public event Action Attacked;

        /// <summary>
        ///     Called from Animation Event
        /// </summary>
        public void ApplyDamageToTarget()
        {
            controller.AudioSource.PlayOneShot(AudioController.Instance.unitMeleeAttack, Random.Range(0.8f, 1f));

            if (!controller.currentTarget || !controller.currentTarget.IsAlive)
                return;

            controller.currentTarget.TakeDamage(controller.UnitData.damage, controller.UnitData.attackType);
            Attacked?.Invoke();
        }
    }
}