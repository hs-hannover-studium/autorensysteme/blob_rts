﻿using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.Combat
{
    public class UnitDamageable : Damageable
    {
        private readonly int _dieAnimID = Animator.StringToHash("Die");
        [SerializeField] private Animator animator;
        [SerializeField] private StateControllerUnit controller;

        private void Start()
        {
            animator = animator ? animator : GetComponent<Animator>();
            controller = controller ? controller : GetComponent<StateControllerUnit>();

            data = controller.UnitData;
        }

        public override void TakeDamage(int value, AttackType attackType = AttackType.None)
        {
            base.TakeDamage(value, attackType);

            // if (Random.Range(0.0f, 1f) < 0.3f)
                // controller.AudioSource.PlayOneShot(AudioController.Instance.unitTakeDamage, Random.Range(0.8f, 1f));
        }

        public override void Kill()
        {
            base.Kill();
            // controller.AudioSource.PlayOneShot(AudioController.Instance.unitDeath, Random.Range(0.8f, 1f));
            animator.SetTrigger(_dieAnimID);
        }
    }
}