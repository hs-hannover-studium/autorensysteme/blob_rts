﻿using Kniblings.Controllers;
using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] protected AudioClip audioClipCollide;
        [SerializeField] private AudioSource audioSource;
        public StateControllerUnit controller;

        private void Update()
        {
            if (Vector3.Distance(controller.transform.position, transform.position) > controller.UnitData.attackRange)
                Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            audioSource.PlayOneShot(AudioController.Instance.projectileFireballExplode, Random.Range(0.8f, 1f));

            if (!other.gameObject.TryGetComponent(out Damageable damageable))
                return;

            damageable.TakeDamage(controller.UnitData.damage, controller.UnitData.attackType);
        }
    }
}