﻿using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.World.Buildings;
using UnityEngine;

namespace Kniblings.Combat
{
    [RequireComponent(typeof(AudioSource))]
    public class BuildingDamageable : Damageable
    {
        private AudioSource audioSource;

        protected override void Awake()
        {
            base.Awake();
            audioSource = GetComponent<AudioSource>();
        }

        public override void TakeDamage(int value, AttackType attackType = AttackType.None)
        {
            base.TakeDamage(value, attackType);

            // if (Random.Range(0.0f, 1f) < 0.3f)
                // audioSource.PlayOneShot(AudioController.Instance.buildingTakeDamage, Random.Range(0.8f, 1f));
        }

        public override void Kill()
        {
            base.Kill();

            GetComponent<BuildingBase>().Demolish();
        }
    }
}