﻿using System;
using Kniblings.Data;
using Kniblings.Data.Enums;
using UnityEditor;
using UnityEngine;

namespace Kniblings.Combat
{
    public abstract class Damageable : MonoBehaviour
    {
        protected int _health;
        protected bool _isAlive = true;
        [SerializeField] protected EntityData data;

        [SerializeField] private bool isInvulnerable;


        public virtual int Health
        {
            get => _health;
            set
            {
                _health = Mathf.Clamp(value, 0, MaxHealth);
                if (_health <= 0)
                    Kill();
            }
        }

        public int MaxHealth { get; private set; }

        public bool IsAlive => _isAlive;

        public event Action<Damageable> Killed;
        public event Action Spawned;
        public event Action Damaged;
        public event Action Healed;

        protected virtual void Awake()
        {
            MaxHealth = data.MaxHealth;
            Health = MaxHealth;
            Spawned?.Invoke();
        }

        /// <summary>
        ///     Applies damage to damageable component
        /// </summary>
        /// <param name="value"></param>
        /// <param name="attackType"></param>
        public virtual void TakeDamage(int value, AttackType attackType = AttackType.None)
        {
            Damaged?.Invoke();

            if (isInvulnerable)
                return;

            int actualDamage = CalculateDamage(value, attackType);
            Health -= actualDamage;
        }

        /// <summary>
        ///     Calculates received damage
        /// </summary>
        /// <param name="value"></param>
        /// <param name="attackType"></param>
        /// <returns></returns>
        protected virtual int CalculateDamage(int value, AttackType attackType = AttackType.None)
        {
            return value;
        }

        public void IncreaseMaxHealth(int value)
        {
            MaxHealth += value;
        }

        public virtual void Heal(int value)
        {
            Health += value;
            Healed?.Invoke();
        }

        public void HealCompletely()
        {
            Heal(MaxHealth);
        }

        public virtual void Kill()
        {
            Killed?.Invoke(this);
            _isAlive = false;
        }

        public void DestroyObject()
        {
            Destroy(gameObject, 3.0f);
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(Damageable), true)]
        public class DamageableEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                Damageable t = target as Damageable;

                GUILayout.BeginHorizontal();

                EditorGUILayout.LabelField($"Health: {t.Health}");
                EditorGUILayout.LabelField($"MaxHealth: {t.MaxHealth}");

                GUILayout.EndHorizontal();


                GUILayout.BeginHorizontal();

                EditorGUILayout.LabelField($"Is Alive: {t.IsAlive}");

                GUILayout.EndHorizontal();

                if (GUILayout.Button("Heal Completely"))
                    t.HealCompletely();
                if (GUILayout.Button("Increase MaxHealth"))
                    t.IncreaseMaxHealth(t.MaxHealth / 100 * 20);
                if (GUILayout.Button("Damage"))
                    t.TakeDamage(t.MaxHealth / 100 * 20);
                if (GUILayout.Button("Heal"))
                    t.Heal(t.MaxHealth / 100 * 20);
                if (GUILayout.Button("Kill"))
                    t.Kill();
            }
        }
#endif
    }
}