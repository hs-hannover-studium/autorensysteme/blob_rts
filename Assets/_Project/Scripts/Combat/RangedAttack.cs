﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Combat
{
    public class RangedAttack : AttackBase
    {
        private Projectile _projectile;
        [SerializeField] [Required] private Projectile projectilePrefab;
        [SerializeField] [Required] private Transform projectileSpawnPoint;
        [SerializeField] [Required] private int collisionLayer;

        public event Action Attacked;

        /// <summary>
        ///     Called from Animation Event
        /// </summary>
        public void SpawnProjectile()
        {
            _projectile = Instantiate(projectilePrefab, projectileSpawnPoint.position,
                transform.rotation);
            
            _projectile.controller = controller;
            _projectile.gameObject.layer = collisionLayer;

            Attacked?.Invoke();
        }
    }
}