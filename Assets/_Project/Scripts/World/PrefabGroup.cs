﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Kniblings.World
{
    [Serializable]
    public struct PrefabGroup
    {
        public PrefabProportionValue[] prefabs;

        public PrefabGroup(PrefabProportionValue[] prefabs)
        {
            this.prefabs = prefabs;
            
        }

        [Button]
        public void NormalizeProportions()
        {
            float sum = 0;
            foreach (PrefabProportionValue p in prefabs)
            {
                sum += p.proportion;
            }

            for (int index = 0; index < prefabs.Length; index++)
            {
                prefabs[index].proportion /= sum;
            }
        }

        public GameObject GetRandom()
        {
            NormalizeProportions();
            float w = Random.value;
            float s = 0;
            foreach (PrefabProportionValue p in prefabs)
            {
                s += p.proportion;
                if (w <= s)
                {
                    return p.prefab;
                }
            }

            return prefabs[0].prefab;
        }

    }

    [Serializable]
    public struct PrefabProportionValue
    {
        public GameObject prefab;
        public float proportion;
    }
}