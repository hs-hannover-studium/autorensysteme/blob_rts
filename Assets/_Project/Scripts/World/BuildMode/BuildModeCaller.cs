﻿using System;
using Kniblings.Data;

namespace Kniblings.World.BuildMode
{
    public static class BuildModeCaller
    {
        public static event Action BuildModeToggle;
        public static event Action BuildModeEntered;
        public static event Action BuildModeLeft;
        public static event Action PlacementModeEntered;
        public static event Action PlacementModeLeft;
        public static event Action<BuildingData> BuilderRequest;

        public static void ModeToggleCall()
        {
            BuildModeToggle?.Invoke();
        }

        public static void ModeEnteredCall()
        {
            BuildModeEntered?.Invoke();
        }

        public static void ModeLeftCall()
        {
            BuildModeLeft?.Invoke();
        }

        public static void BuilderRequestCall(BuildingData data)
        {
            BuilderRequest?.Invoke(data);
        }

        public static void PlacementModeEnteredCall()
        {
            PlacementModeEntered?.Invoke();
        }

        public static void PlacementModeLeftCall()
        {
            PlacementModeLeft?.Invoke();
        }
    }
}