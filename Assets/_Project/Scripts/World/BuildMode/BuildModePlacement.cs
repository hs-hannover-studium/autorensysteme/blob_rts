﻿using System;
using System.Collections;
using DG.Tweening;
using Kniblings.Controllers;
using Kniblings.CustomInput;
using Kniblings.Data;
using Kniblings.Selection;
using Kniblings.World.Buildings;
using UnityEngine;

namespace Kniblings.World.BuildMode
{
    public class BuildModePlacement : MonoBehaviour
    {
        public static event Action BuildingBuild;
        
        private void OnEnable()
        {
            InputController.Actions.leftMouse.Down += OnPlaceBuilding;
            InputController.Actions.middleMouse.Down += AbortPlacement;
            InputController.Actions.buildingRotateCW.Down += OnBuildingRotateClockwise;
            InputController.Actions.buildingRotateCCW.Down += OnBuildingRotateCounter;
        }

        private void OnDisable()
        {
            InputController.Actions.leftMouse.Down -= OnPlaceBuilding;
            InputController.Actions.middleMouse.Down -= AbortPlacement;
            InputController.Actions.buildingRotateCW.Down -= OnBuildingRotateClockwise;
            InputController.Actions.buildingRotateCCW.Down -= OnBuildingRotateCounter;
        }

        public void StartPlacement(BuildingData data, Action actionCallback)
        {
            enabled = true;
            CastRay();
            InstantiateBuilding(data.Prefab);
            _callback = actionCallback;
            _buildingCost = data.ResourceCost;
            BuildModeCaller.PlacementModeEnteredCall();
        }

        public void StopPlacement()
        {
            enabled = false;
            StartCoroutine(DelayModeLeftCall(0.1f));
            _activeTile.HighlightTile(false);
        }

        public IEnumerator DelayModeLeftCall(float time)
        {
            yield return new WaitForSeconds(time);
            BuildModeCaller.PlacementModeLeftCall();
        }

        public void AbortPlacement()
        {
            if (enabled)
            {
                StopPlacement();
                _currentBuildingContainer.DestroyContainer(0f);
            }
        }

        private void OnPlaceBuilding()
        {
            if (PlaceOn(_activeTile))
            {
                StopPlacement();
                BuildingBuild?.Invoke();    
            }
        }

        private void OnBuildingRotateClockwise()
        {
            Rotate(rotationIncrement);
        }

        private void OnBuildingRotateCounter()
        {
            Rotate(-rotationIncrement);
        }

        private void FixedUpdate()
        {
            BuildModeUpdate();
        }

        private void InstantiateBuilding(GameObject prefab)
        {
            _currentBuildingTransform =
                Instantiate(prefab, _realtimeHit.point + floatingOffset,
                    Quaternion.identity).transform;

            _currentBuildingContainer = _currentBuildingTransform.GetComponent<BuildingContainer>();
            _currentBuildingContainer.SetRotation(new Vector3(0, _yRotation, 0));
        }

        private bool PlaceOn(Tile tile)
        {
            if (!tile.PlaceObject(_currentBuildingContainer) |
                !PlayerController.Instance.CompareResources(_buildingCost))
                return false;

            _currentBuildingContainer.transform.DOMove(tile.transform.position, snappingDuration).SetEase(snappingStyle)
                .OnComplete(() =>
                    _currentBuildingContainer.BuildCompleteCall(tile));
            _currentBuildingContainer.transform.DOLocalRotateQuaternion(
                    Quaternion.Euler(0, 0, 0), snappingDuration)
                .SetEase(snappingStyle);

            _callback.Invoke();
            return true;
        }

        private void OnNewTileHit(Tile tile)
        {
            try
            {
                _activeTile.HighlightTile(false);
            }
            catch (NullReferenceException e)
            {
                // ignored
                // would be thrown on first call because _activeTile has not been assigned yet
            }

            _activeTile = tile;
            _activeTile.HighlightTile(true);
        }

        private void Rotate(float angle)
        {
            if (_currentBuildingContainer.Rotate(new Vector3(0, _yRotation + angle, 0), rotationDuration, rotationEase))
                _yRotation += angle;
        }

        private bool CastRay()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            return Physics.Raycast(ray, out _realtimeHit, Mathf.Infinity, gridLayerMask);
        }

        #region Variables

        [SerializeField] private Camera cam;
        [SerializeField] private LayerMask gridLayerMask;

        [Header("Floating Building Movement")]
        [SerializeField]
        private Vector3 floatingOffset;

        [SerializeField] private float rotationIncrement = 60f;
        [SerializeField] private Ease rotationEase = Ease.InCubic;
        [SerializeField] private float rotationDuration = 0.3f;


        [SerializeField] private float positionDrag;
        [SerializeField] private float wiggleScale;
        [SerializeField] private float wiggleClamp;
        [SerializeField] private float wiggleDrag;

        [Header("Tile Snap")] [SerializeField] private float snappingDuration = 1f;

        [SerializeField] private Ease snappingStyle = Ease.Linear;

        private Transform _currentBuildingTransform;
        private BuildingContainer _currentBuildingContainer;

        //TODO method callback to add action to undo list
        private Action _callback;

        private Tile _activeTile;
        private float _yRotation;
        private RaycastHit _realtimeHit;
        private RaycastHit _lastTileHit;

        private ResourcePackage _buildingCost;

        #endregion

        #region BuildModeUpdate

        private void BuildModeUpdate()
        {
            if (!CastRay())
                return;

            MoveActiveBuilding(_realtimeHit.point + floatingOffset);

            if (_realtimeHit.transform == _lastTileHit.transform) return;

            if (_realtimeHit.transform.TryGetComponent(out Tile tile))
            {
                OnNewTileHit(tile);
                _lastTileHit = _realtimeHit;
            }
        }


        private void MoveActiveBuilding(Vector3 destination)
        {
            Vector3 pos = _currentBuildingTransform.position;

            _currentBuildingTransform.position = Vector3.Lerp(pos, destination, positionDrag);
            Vector3 rotation = (destination - pos) * wiggleScale;

            rotation = new Vector3(-Mathf.Clamp(rotation.z, -wiggleClamp, wiggleClamp), 0,
                Mathf.Clamp(rotation.x, -wiggleClamp, wiggleClamp));

            _currentBuildingTransform.localRotation = Quaternion.Lerp(_currentBuildingTransform.localRotation,
                Quaternion.Euler(rotation), wiggleDrag);
        }

        #endregion
    }
}