﻿using System;
using Kniblings.Controllers;
using UnityEngine;

namespace Kniblings.World.BuildMode
{
    public class BuildingGrid : MonoBehaviour
    {
        private void Start()
        {
            BuildModeCaller.BuildModeEntered += BuildBuildModeCallerOnBuildModeEntered;
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            BuildModeCaller.BuildModeEntered -= BuildBuildModeCallerOnBuildModeEntered;
            BuildModeCaller.BuildModeLeft -= BuildBuildModeCallerOnBuildModeLeft;
        }

        private void BuildBuildModeCallerOnBuildModeLeft()
        {
            gameObject.SetActive(false);
            BuildModeCaller.BuildModeLeft -= BuildBuildModeCallerOnBuildModeLeft;
        }

        private void BuildBuildModeCallerOnBuildModeEntered()
        {
            gameObject.SetActive(true);
            BuildModeCaller.BuildModeLeft += BuildBuildModeCallerOnBuildModeLeft;
        }
    }
}