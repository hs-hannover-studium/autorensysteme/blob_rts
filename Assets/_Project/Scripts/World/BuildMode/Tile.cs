﻿using Kniblings.World.Buildings;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.World.BuildMode
{
    public class Tile : MonoBehaviour
    {
        private BuildingContainer _buildingContainer;

        private GameObject _particles;

        [ShowInInspector] [ReadOnly] public bool Occupied { get; private set; }

        private void Awake()
        {
            _particles = transform.GetChild(0).gameObject;
        }

        public bool PlaceObject(BuildingContainer buildingContainer)
        {
            if (Occupied)
                return false;

            _buildingContainer = buildingContainer;
            Occupied = true;
            return true;
        }

        public void RemoveObject()
        {
            if (!Occupied)
                return;

            Occupied = false;
        }

        public void HighlightTile(bool state)
        {
            _particles.SetActive(state);
        }
    }
}