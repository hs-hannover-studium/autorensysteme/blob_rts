﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kniblings.Data.Enums;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Experimental.TerrainAPI;
using Random = UnityEngine.Random;

namespace Kniblings.World
{
    public class ResourceSpawner : MonoBehaviour
    {
        [Tooltip("Ordered by channel -> r, g, b, a")]
        public PrefabGroup[] prefabGroups;
        
        public Texture2D distributionMap;
        public int maxCollectiblesOnMap;
        [Tooltip("Only start respawning resources once this many have been collected")]
        public int respawnCountOffset;
        public float tickRate;
        public Terrain terrain;
        public LayerMask terrainLayer;
        public LayerMask obstacleMask;
        public float obstructionRadius = 1f;

        [ReadOnly, ShowInInspector]
        private int _collectiblesOnMap;
        private Bounds _bounds;
        private Collider[] obstacles = new Collider[1];
        private Coroutine _spawnerRoutine;

        public static event Action<ResourceCollectible> ResourceCollected; 

        private void OnEnable()
        {
            _bounds = terrain.terrainData.bounds;
            // _spawnerRoutine = StartCoroutine(SpawnerRoutine());
            while (_collectiblesOnMap < maxCollectiblesOnMap)
            {
                // Choose random resource
                int i = Random.Range(0, prefabGroups.Length);

                SpawnResourceOnChannelRecursive(i);
                _collectiblesOnMap++;

            }
            ResourceCollected += OnResourceCollected;
            // Debug.Log($"center: {_bounds.center}, min: {_bounds.min}, max: {_bounds.max}, size: {_bounds.size}");
        }


        private void OnDisable()
        {
            StopAllCoroutines();
            ResourceCollected -= OnResourceCollected;
        }
        

        public static void ResourceCollectedCall(ResourceCollectible rc)
        {
            ResourceCollected?.Invoke(rc);
        }
        
        private void OnResourceCollected(ResourceCollectible resource)
        {
            _collectiblesOnMap--;
            if (_spawnerRoutine == null && _collectiblesOnMap + respawnCountOffset < maxCollectiblesOnMap)
                _spawnerRoutine = StartCoroutine(SpawnerRoutine());
        }

        private IEnumerator SpawnerRoutine()
        {
            while (_collectiblesOnMap < maxCollectiblesOnMap)
            {
                // Choose random resource
                int i = Random.Range(0, prefabGroups.Length);

                SpawnResourceOnChannelRecursive(i);
                _collectiblesOnMap++;

                yield return new WaitForSeconds(1 / tickRate);
            }

            _spawnerRoutine = null;
        }

        private bool SpawnResourceOnChannel(int channel)
        {
            // Sample random position 
            Vector2 samplePos = new Vector2(Random.value, Random.value);
            Color sampleColor = distributionMap.GetPixelBilinear(samplePos.x, samplePos.y);

            // Sample value on distribution map and compare to random value 
            float w = Random.value;
            if (sampleColor[channel] <= w)
                return false;

            //TODO should use raycasting 
            Vector3 p0 = new Vector3(_bounds.min.x + samplePos.x * _bounds.size.x, 0,
                _bounds.min.z + samplePos.y * _bounds.size.z);

            RaycastHit raycastHit;
            if (!Physics.Raycast(p0 + Vector3.up * 100f, Vector3.down, out raycastHit, 200f, terrainLayer))
                return false;
            
            Vector3 p = raycastHit.point;
            Debug.DrawRay(p, Vector3.up * 5f, Color.blue, 2f);

            if (AreaObstructed(p, obstructionRadius))
                return false;
            
            Quaternion randomRotation = Quaternion.Euler(0, Random.Range(0, 360f), 0);

            Instantiate(prefabGroups[channel].GetRandom(), p, randomRotation, transform);
            return true;
        }


        /// <summary>
        /// Guarantees to spawn a resource by looking for a valid position
        /// </summary>
        /// <param name="channel"></param>
        private void SpawnResourceOnChannelRecursive(int channel)
        {
            bool b = false;
            while (!b)
            {
                b = SpawnResourceOnChannel(channel);
            }
        }

        /// <summary>
        /// Check if the area is obstacle free, ignoring Triggers
        /// </summary>
        /// <param name="position"></param>
        /// <param name="radius"></param>
        /// <returns>True if no obstacle found</returns>
        private bool AreaObstructed(Vector3 position, float radius)
        {
            int count = Physics.OverlapSphereNonAlloc(position, radius, obstacles, obstacleMask, QueryTriggerInteraction.Ignore);
            return count > 0;

        }
        
#if UNITY_EDITOR
        [Button]
        private void CountResources()
        {
            ResourceCollectible[] r = GetComponentsInChildren<ResourceCollectible>();
            int s = 0;
            int s2 = 0;
            int w = 0;
            int w2 = 0;
            foreach (ResourceCollectible collectible in r)
            {
                if (collectible.Resource.type == ResourceType.Stone)
                {
                    s++;
                    s2 += collectible.Resource.value;
                }      
                else
                {
                    w++;
                    w2 += collectible.Resource.value;
                }
            }

            Debug.Log($"Stone: {s} ({s2}), Wood: {w} ({w2})");
        }        
#endif
        
        
        

    }
}