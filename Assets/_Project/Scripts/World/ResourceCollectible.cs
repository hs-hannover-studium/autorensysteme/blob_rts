﻿using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.StateMachine.Unit;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.World
{
    public class ResourceCollectible : MonoBehaviour
    {
        [SerializeField] private ResourceTypeValue resource;
        
        public ResourceTypeValue Resource => resource;
        
        [ShowInInspector, ReadOnly]
        public StateControllerVillager FoundBy { get; set; }

        public void PickUp(StateControllerVillager controller)
        {
            controller.backpack += resource.ToResourcePackage();
            ResourceSpawner.ResourceCollectedCall(this);
            Destroy(gameObject);
        }
    }
}