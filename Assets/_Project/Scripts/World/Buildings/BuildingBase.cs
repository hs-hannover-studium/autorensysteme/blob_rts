﻿using Kniblings.Data;
using Kniblings.Selection;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.World.Buildings
{
    public abstract class BuildingBase : MonoBehaviour
    {
        protected BuildingContainer container;

        [AssetsOnly] public BuildingData data;

        protected SelectableBuilding selectableBuilding;


        protected virtual void Awake()
        {
            container = transform.parent.GetComponent<BuildingContainer>();
            selectableBuilding = GetComponent<SelectableBuilding>();
        }

        public abstract void Demolish();
    }
}