﻿using System;
using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Other;
using Kniblings.Utility;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.World.Buildings
{
    public class VillageCore : BuildingBase
    {
        public Transform gatherPoint;

        [AssetsOnly] public Knibling kniblingPrefab;

        public Transform spawnPoint;
        public UnitData unitData;

        public static event Action KniblingCreated;

        private Coroutine _malisiumCoroutine;

        public override void Demolish()
        {
            //indestructible
        }

        public void SpawnVillager()
        {
            if (PlayerController.Instance.kniblings.RuntimeValue.Count >= PlayerController.Instance.kniblingsLimit)
            {
                PlayerController.Instance.OnKniblingsLimitReached();
                return;
            }

            if (PlayerController.Instance.SpendResources(unitData.ResourceCost))
                SpawnVillagerInternal(1);
        }

        [Button("Spawn Villager")]
        [ShowIf("@UnityEngine.Application.isPlaying")]
        private void SpawnVillagerInternal(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                Spawner.Spawn(kniblingPrefab, spawnPoint.position, gatherPoint.position, 1f);
                KniblingCreated?.Invoke();
            }
                
        }
    }
}