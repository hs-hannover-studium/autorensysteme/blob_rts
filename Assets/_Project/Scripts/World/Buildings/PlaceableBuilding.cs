﻿using System.Collections.Generic;
using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Other;
using Kniblings.StateMachine.Unit;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

namespace Kniblings.World.Buildings
{
    public class PlaceableBuilding : BuildingBase
    {
        public int layerOnPlace;
        public GameObject[] obstacles;
        [SerializeField] private Transform gatherPoint;
        [SerializeField] private GameObject demolishParticles;

        [ShowInInspector]
        public List<StateControllerUnit> AssignedUnits { get; } = new List<StateControllerUnit>();

        public PlaceableBuildingData SpecialData => (PlaceableBuildingData) data;

        public Transform GatherPoint => gatherPoint;

        private void OnEnable()
        {
            container.BuildComplete += ContainerOnBuildComplete;
        }

        private void OnDisable()
        {
            container.BuildComplete -= ContainerOnBuildComplete;
        }

        protected void ContainerOnBuildComplete()
        {
            PlayerController.Instance.buildings.Add(this);
            PlayerController.Instance.SpendResources(data.ResourceCost);
            gameObject.layer = layerOnPlace;
            ActivateObstacles();
            CheckForUnassignedUnits();
        }

        private void ActivateObstacles()
        {
            foreach (GameObject obstacle in obstacles)
            {
                obstacle.SetActive(true);
            }
        }

        // Checks for any unassigned units running around to prevent exploits
        private void CheckForUnassignedUnits()
        {
            foreach (Knibling knibling in PlayerController.Instance.kniblings.RuntimeValue)
            {
                if (knibling.assignedBuilding == null && knibling.CurrentJob == SpecialData.jobType)
                {
                    knibling.assignedBuilding = this;
                    AssignedUnits.Add(knibling.StateController);
                }
            }
        }

        public bool CanAssignUnit(StateControllerUnit unit)
        {
            return (AssignedUnits.Count < SpecialData.capacity) & !AssignedUnits.Contains(unit);
        }

        public override void Demolish()
        {
            PlayerController.Instance.buildings.Remove(this);
            AudioSource.PlayClipAtPoint(AudioController.Instance.buildingDemolish, transform.position);
            Instantiate(demolishParticles, transform.position, Quaternion.identity, transform.parent);
            gameObject.SetActive(false);
            container.DemolishBuilding(1f);
        }
    }
}