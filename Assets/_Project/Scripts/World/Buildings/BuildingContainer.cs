﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Kniblings.Controllers;
using Kniblings.World.BuildMode;
using UnityEngine;

namespace Kniblings.World.Buildings
{
    public class BuildingContainer : MonoBehaviour
    {
        public LayerMask obstructionLayers;
        public float obstructionRadius;

        public AudioSource audioSource;
        
        private bool _rotating;
        private Tile _tile;

        private List<GameObject> _hiddenObstructions = new List<GameObject>();


        /// <summary>
        ///     Gets called once the building is snapped to the tile
        /// </summary>
        public event Action BuildComplete;

        /// <summary>
        ///     Rotate the object during BuildMode
        /// </summary>
        /// <param name="rotation">Rotation increment</param>
        /// <param name="duration">Duration of the tween</param>
        /// <param name="ease">Ease type for the tween</param>
        /// <returns>True if successful, false if not (already rotating)</returns>
        public bool Rotate(Vector3 rotation, float duration, Ease ease)
        {
            if (_rotating)
                return false;

            _rotating = true;
            transform.GetChild(0).DOLocalRotateQuaternion(Quaternion.Euler(rotation), duration).SetEase(ease)
                .OnComplete(() => _rotating = false);
            return true;
        }

        /// <summary>
        ///     Set the rotation of the object to the one supplied
        /// </summary>
        /// <param name="rotation"></param>
        public void SetRotation(Vector3 rotation)
        {
            if (_rotating)
                return;
            transform.GetChild(0).localRotation = Quaternion.Euler(rotation);
        }

        /// <summary>
        ///     Destroy the container object silently and immediately
        /// </summary>
        public void DestroyContainer(float delay)
        {
            ReactivateHiddenObstructions();
            Destroy(gameObject, delay);
        }

        public void DemolishBuilding(float delay)
        {
            _tile.RemoveObject();
            // AudioController.Instance.PlayFxOneShot(AudioController.Instance.buildingDemolish);
            DestroyContainer(delay);
        }

        /// <summary>
        ///     Call to indicate that the building has been placed on a tile
        /// </summary>
        public void BuildCompleteCall(Tile tile)
        {
            _tile = tile;
            ClearObstructions();
            AudioController.Instance.PlayFxOneShot(AudioController.Instance.buildingPlaced);
            BuildComplete?.Invoke();
        }

        public void ClearObstructions()
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, obstructionRadius, obstructionLayers);
            foreach (Collider c in colliders)
            {
                if (c.CompareTag("ObstructionHide"))
                {
                    _hiddenObstructions.Add(c.gameObject);
                    c.gameObject.SetActive(false);
                } else if (c.CompareTag("ObstructionDestroy"))
                {
                    Destroy(c.gameObject);
                }
            }
        }

        public void ReactivateHiddenObstructions()
        {
            foreach (GameObject o in _hiddenObstructions)
            {
                o.SetActive(true);
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, obstructionRadius);
        }
    }
}