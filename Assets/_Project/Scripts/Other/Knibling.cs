﻿using System;
using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Locomotion;
using Kniblings.Selection;
using Kniblings.StateMachine;
using Kniblings.StateMachine.Unit;
using Kniblings.World.Buildings;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.Other
{
    public class Knibling : MonoBehaviour
    {
        public PlaceableBuilding assignedBuilding;

        private JobType _currentJob;
        private Vector3 _prevPosition;
        public JobPrefabsData prefabsData;

        public StateControllerUnit StateController { get; private set; }

        public JobType CurrentJob => _currentJob;

        private void OnEnable()
        {
            RegisterAtPlayerController();
        }

        private void OnDisable()
        {
            UnregisterAtPlayerController();
        }
        
        public void Init(Vector3 gatherPosition, float randomRadius)
        {
            _currentJob = JobType.None;
            _prevPosition = transform.position;
            SwapPrefab(prefabsData.villagerPrefab);
            if (!UnitMovement.RandomPosInsideSphere(randomRadius, gatherPosition, out Vector3 location))
                StateController.moveDestination = gatherPosition;
            else
                StateController.moveDestination = location;

            RegisterAtPlayerController();
        }

        public Vector3 GetPosition()
        {
            return GetTransform().position;
        }

        public Transform GetTransform()
        {
            return transform.GetChild(0);
        }

        private void SwapPrefab(GameObject prefab)
        {
            try
            {
                _prevPosition = GetPosition();
                StateController.UnitKilled -= StateControllerOnUnitKilled;
                Destroy(transform.GetChild(0).gameObject);
            }
            catch (Exception e)
            {
                //ignored on first instatiation
            }

            GameObject o = Instantiate(prefab, _prevPosition, Quaternion.identity, transform);
            StateController = o.GetComponent<StateControllerUnit>();
            StateController.UnitKilled += StateControllerOnUnitKilled;
        }

        private void StateControllerOnUnitKilled()
        {
            try
            {
                assignedBuilding.AssignedUnits.Remove(StateController);
            }
            catch (Exception e)
            {
                //ignore
            }
            UnregisterAtPlayerController();
            Destroy(gameObject, 10f);
        }

        private void RegisterAtPlayerController()
        {
            if (PlayerController.Instance.kniblings.RuntimeValue.Contains(this))
                return;

            PlayerController.Instance.kniblings.Add(this);
        }

        private void UnregisterAtPlayerController()
        {
            PlayerController.Instance.kniblings.Remove(this);
        }

        public void ChangeJob(JobType jobType)
        {
            if (jobType == CurrentJob)
            {
                SelectedObjects.Deselect(StateController.Selectable);
                if (assignedBuilding != null)
                {
                    assignedBuilding.AssignedUnits.Remove(StateController);
                }
                
                ChangeJobInternal(JobType.None);
                
                if (!UnitMovement.RandomPosInsideSphere(1f, PlayerController.Instance.villageCore.position,
                    out StateController.moveDestination))
                    StateController.moveDestination = PlayerController.Instance.villageCore.position;

                StateController.TransitionToState(StateController.UnitData.defaultMoveState);
                
                return;
            }
            
            foreach (PlaceableBuilding building in PlayerController.Instance.buildings)
            {
                if (building.SpecialData.jobType != jobType || !building.CanAssignUnit(StateController)) continue;
                
                
                SelectedObjects.Deselect(StateController.Selectable);
                if (assignedBuilding != null)
                {
                    assignedBuilding.AssignedUnits.Remove(StateController);
                }
                    
                // unit gets destroyed in here
                ChangeJobInternal(jobType);

                building.AssignedUnits.Add(StateController);
                assignedBuilding = building;
                    
                if (!UnitMovement.RandomPosInsideSphere(1f, building.GatherPoint.position,
                    out StateController.moveDestination))
                    StateController.moveDestination = building.GatherPoint.position;

                StateController.TransitionToState(StateController.UnitData.defaultMoveState);
                break;
            }
        }

        [Button]
        private void ChangeJobInternal(JobType job)
        {
            switch (job)
            {
                case JobType.None:
                    SwapPrefab(prefabsData.villagerPrefab);
                    break;
                case JobType.Warrior:
                    SwapPrefab(prefabsData.warriorPrefab);
                    break;
                case JobType.Mage:
                    SwapPrefab(prefabsData.magePrefab);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(job), job, null);
            }
            _currentJob = job;
        }

    }
}