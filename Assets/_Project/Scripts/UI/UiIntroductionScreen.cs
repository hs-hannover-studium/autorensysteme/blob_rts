﻿using Kniblings.Controllers;
using Kniblings.Data.Values;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiIntroductionScreen : MonoBehaviour
    {
        public GameObject introductionCanvas;
        public BoolValue introductionShown;
        public GameObject pauseMenu;

        private void Start()
        {
            introductionCanvas.SetActive(false);
            if (introductionShown.RuntimeValue)
                return;

            GameController.Instance.PauseGame();
            pauseMenu.SetActive(false);
            introductionCanvas.SetActive(true);
        }
        
        public void OnButtonClicked()
        {
            introductionShown.RuntimeValue = true;
            GameController.Instance.ResumeGame();
        }
    }
}