﻿using System.Collections.Generic;
using Kniblings.Selection;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Selectable = Kniblings.Selection.Selectable;

namespace Kniblings.UI
{
    public class UiUnitGrid : MonoBehaviour
    {
        [SerializeField] private int maxCount = 8;
        [SerializeField] private GameObject unitIconPrefab;
        [SerializeField] private GameObject unitIconMorePrefab;

        public void Init(Selectable[] units)
        {
            for (int i = 0; i < units.Length; i++)
            {
                if (i >= maxCount - 1)
                {
                    GameObject more = Instantiate(unitIconMorePrefab, transform);
                    more.GetComponent<UiUnitIconMore>().Init(units.Length - maxCount + 1);
                    break;
                }

                SelectableUnit unit = (SelectableUnit) units[i];
                GameObject uip = Instantiate(unitIconPrefab, transform);
                uip.GetComponent<UiUnitIcon>().Init(unit.StateController.UnitData, unit);
            }
        }

        public void Clear()
        {
            List<GameObject> c = new List<GameObject>();
            foreach (Transform t in transform) c.Add(t.gameObject);

            foreach (GameObject o in c) Destroy(o);
        }
    }
}