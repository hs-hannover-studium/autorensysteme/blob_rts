using System.Collections.Generic;
using Kniblings.Selection;
using Kniblings.StateMachine.Unit;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiSelectionInfoUnit : MonoBehaviour
    {
        private SelectableUnit _selectable;
        public TextMeshProUGUI descriptionText;
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI damageText;
        public TextMeshProUGUI maxHpText;
        public TextMeshProUGUI attackRangeText;
        public TextMeshProUGUI attackRateText;

        private void OnEnable()
        {
            _selectable = (SelectableUnit) SelectedObjects.Selected[0];

            nameText.SetText(_selectable.StateController.UnitData.Name);
            descriptionText.SetText(_selectable.StateController.UnitData.Description);
            damageText.SetText(_selectable.StateController.UnitData.damage.ToString());
            maxHpText.SetText(_selectable.StateController.UnitData.MaxHealth.ToString());
            attackRangeText.SetText(_selectable.StateController.UnitData.attackRange.ToString());
            attackRateText.SetText(_selectable.StateController.UnitData.attackRate.ToString());

            foreach (UiJobAssignment component in GetComponentsInChildren<UiJobAssignment>())
                component.Init(new List<StateControllerUnit> {_selectable.StateController});
        }

        private void OnDisable()
        {
            _selectable = null;
            nameText.SetText("");
            descriptionText.SetText("");
            damageText.SetText("");
            maxHpText.SetText("");
            attackRangeText.SetText("");
            attackRateText.SetText("");
        }
    }
}