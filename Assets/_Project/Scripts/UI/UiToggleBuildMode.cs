﻿using Kniblings.World.BuildMode;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiToggleBuildMode : MonoBehaviour
    {
        public void ToggleBuildMode()
        {
            BuildModeCaller.ModeToggleCall();
        }
    }
}