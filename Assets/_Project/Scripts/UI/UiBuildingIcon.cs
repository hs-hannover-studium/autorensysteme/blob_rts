﻿using Kniblings.Data;
using Kniblings.World.BuildMode;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Kniblings.UI
{
    public class UiBuildingIcon : MonoBehaviour
    {
        private BuildingData _data;

        [SerializeField] private Image image;
        [SerializeField] private new TextMeshProUGUI name;
        [SerializeField] private TextMeshProUGUI malisiumTxt;
        [SerializeField] private TextMeshProUGUI woodTxt;
        [SerializeField] private TextMeshProUGUI stoneTxt;

        public void InitializeIcon(BuildingData data)
        {
            _data = data;
            
            if (_data.Icon != null) 
                image.sprite = _data.Icon;
            
            name.SetText(_data.Name);

            malisiumTxt.SetText(_data.ResourceCost.malisium > 0 ? _data.ResourceCost.malisium.ToString() : "0");
            woodTxt.SetText(_data.ResourceCost.wood > 0 ? _data.ResourceCost.wood.ToString() : "0");
            stoneTxt.SetText(_data.ResourceCost.stone > 0 ? _data.ResourceCost.stone.ToString() : "0");
        }

        public void OnIconClicked()
        {
            BuildModeCaller.BuilderRequestCall(_data);
        }
    }
}