﻿using Kniblings.Combat;
using UnityEngine;
using UnityEngine.UI;
using Selectable = Kniblings.Selection.Selectable;

namespace Kniblings.UI
{
    public class UiHealthBar : MonoBehaviour
    {
        public Damageable damageable;
        public Image fill;
        public Gradient gradient;
        public GameObject healthbar;
        public Selectable selectable;
        public Slider slider;

        private void Start()
        {
            SetMaxValue(damageable.MaxHealth);
            healthbar.SetActive(false);
        }

        private void OnEnable()
        {
            damageable.Damaged += OnDamaged;
            damageable.Killed += OnKilled;
            selectable.Selected += OnSelected;
            selectable.Deselected += OnDeselected;
        }

        private void OnDisable()
        {
            damageable.Damaged -= OnDamaged;
            damageable.Killed -= OnKilled;
            selectable.Selected -= OnSelected;
            selectable.Deselected -= OnDeselected;
        }

        private void OnKilled(Damageable damageable)
        {
            healthbar.SetActive(false);
        }

        private void OnDamaged()
        {
            SetHealth(damageable.Health);
        }

        private void OnSelected()
        {
            healthbar.SetActive(true);
        }

        private void OnDeselected()
        {
            healthbar.SetActive(false);
        }

        public void SetMaxValue(int value)
        {
            slider.maxValue = value;
            slider.value = value;

            fill.color = gradient.Evaluate(1f);
        }

        public void SetHealth(int health)
        {
            slider.value = health;
            fill.color = gradient.Evaluate(slider.normalizedValue);
        }
    }
}