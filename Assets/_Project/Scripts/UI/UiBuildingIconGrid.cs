﻿using System;
using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.World.BuildMode;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiBuildingIconGrid : MonoBehaviour
    {

        public GameObject foldoutArea;

        public GameObject grid;
        public GameObject iconPrefab;
        public PlaceableBuildingsData placeableBuildingsData;

        private void OnEnable()
        {
            BuildModeCaller.BuildModeEntered += BuildModeCallerOnBuildModeEntered;
        }

        private void OnDisable()
        {
            BuildModeCaller.BuildModeEntered -= BuildModeCallerOnBuildModeEntered;
        }

        private void Start()
        {
            foldoutArea.SetActive(false);

            foreach (BuildingData buildingData in placeableBuildingsData.buildings)
            {
                GameObject icon = Instantiate(iconPrefab, grid.transform);
                icon.GetComponent<UiBuildingIcon>().InitializeIcon(buildingData);
            }

            foldoutArea.transform.localScale = new Vector3(1, 0, 1);
        }

        private void BuildModeCallerOnBuildModeEntered()
        {
            BuildModeCaller.BuildModeLeft += BuildModeCallerOnBuildModeLeft;
            GameController.Instance.GamePaused += BuildModeCallerOnBuildModeLeft;
            foldoutArea.SetActive(true);
        }

        private void BuildModeCallerOnBuildModeLeft()
        {
            BuildModeCaller.BuildModeLeft -= BuildModeCallerOnBuildModeLeft;
            GameController.Instance.GamePaused -= BuildModeCallerOnBuildModeLeft;
            foldoutArea.SetActive(false);
        }
    }
}