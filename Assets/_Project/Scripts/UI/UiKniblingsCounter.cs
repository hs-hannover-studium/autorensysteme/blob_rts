﻿using System;
using System.Collections.Generic;
using Kniblings.Controllers;
using Kniblings.Data.Values;
using Kniblings.Other;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiKniblingsCounter : MonoBehaviour
    {
        public KniblingsValue kniblings;
        public TextMeshProUGUI txt;

        private void OnEnable()
        {
            txt.SetText(kniblings.RuntimeValue.Count + "/" + PlayerController.Instance.kniblingsLimit);
            kniblings.ValueChanged += OnValueChanged;
        }

        private void OnDisable()
        {
            kniblings.ValueChanged -= OnValueChanged;
        }

        private void OnValueChanged(List<Knibling> k)
        {
            txt.SetText(k.Count + "/" + PlayerController.Instance.kniblingsLimit);
        }
    }
}