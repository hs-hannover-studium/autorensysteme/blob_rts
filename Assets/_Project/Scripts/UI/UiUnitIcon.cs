﻿using Kniblings.Data;
using UnityEngine;
using UnityEngine.UI;
using Selectable = Kniblings.Selection;

namespace Kniblings.UI
{
    public class UiUnitIcon : MonoBehaviour
    {
        private UnitData _data;
        private Image _img;
        private Selectable.Selectable _selectable;

        public void Init(UnitData data, Selectable.Selectable selectable)
        {
            _data = data;
            _selectable = selectable;
            if (_data.Icon != null)
            {
                _img = GetComponent<Image>();
                _img.sprite = _data.Icon;
            }
        }

        public void OnButton()
        {
            Selectable.SelectedObjects.DeselectAll();
            Selectable.SelectedObjects.Add(_selectable);
        }
    }
}