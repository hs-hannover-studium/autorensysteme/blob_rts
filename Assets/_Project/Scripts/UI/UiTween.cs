﻿using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UiTween : MonoBehaviour
    {
        private CanvasGroup _canvasGroup;
        private RectTransform _rectTransform;
        public TweenMode mode = TweenMode.Move;
        public Ease easeType = Ease.Linear;
        public Vector3 from;
        public Vector3 to;
        public float delay;
        public float duration = .25f;
        public bool fadeIn = true;
        public bool hideAfterDuration;
        [ShowIf("hideAfterDuration")] public float visibleTime;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvasGroup = GetComponent<CanvasGroup>();
            if (fadeIn)
                _canvasGroup.alpha = 0;
        }

        private void OnEnable()
        {
            if (hideAfterDuration)
                StartCoroutine(SelfHide());

            if (fadeIn)
            {
                _canvasGroup.alpha = 0;
                _canvasGroup.DOFade(1f, duration).SetEase(easeType).SetDelay(delay);
            }

            switch (mode)
            {
                case TweenMode.Move:
                    Move();
                    break;
                case TweenMode.Scale:
                    Scale();
                    break;
                case TweenMode.LoopMove:
                    LoopMove();
                    break;
                case TweenMode.LoopScale:
                    LoopScale();
                    break;
            }
        }

        public void Move()
        {
            _rectTransform.anchoredPosition = from;
            _rectTransform.DOAnchorPos(to, duration).SetEase(easeType).SetDelay(delay);
        }

        public void Scale()
        {
            _rectTransform.localScale = from;
            _rectTransform.DOScale(to, duration).SetEase(easeType).SetDelay(delay);
        }

        public void LoopMove()
        {
            _rectTransform.DOAnchorPos(to, duration).SetEase(easeType).SetDelay(delay)
                .SetLoops(-1, LoopType.Yoyo);
        }

        public void LoopScale()
        {
            _rectTransform.DOScale(to, duration).SetEase(easeType).SetDelay(delay).SetLoops(-1, LoopType.Yoyo);
        }

        private IEnumerator SelfHide()
        {
            yield return new WaitForSeconds(visibleTime);
            gameObject.SetActive(false);
        }
    }

    public enum TweenMode
    {
        Move,
        Scale,
        LoopMove,
        LoopScale
    }
}