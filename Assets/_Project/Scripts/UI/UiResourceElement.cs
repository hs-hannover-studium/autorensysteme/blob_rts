﻿using Kniblings.Data.Enums;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Kniblings.UI
{
    public class UiResourceElement : MonoBehaviour
    {
        private TextMeshProUGUI _countText;
        private Image _icon;
        public ResourceType type;

        private void Awake()
        {
            _icon = GetComponentInChildren<Image>();
            _countText = GetComponentInChildren<TextMeshProUGUI>();
        }

        public void UpdateCount(int value)
        {
            _countText.SetText(value.ToString());
        }
    }
}