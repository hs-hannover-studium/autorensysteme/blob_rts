﻿using Kniblings.Selection;
using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiChangeAggressionMode : MonoBehaviour
    {
        public void MakeAggressive()
        {
            foreach (Selectable s in SelectedObjects.Selected)
            {
                if (!s.gameObject.TryGetComponent(out StateControllerUnit controller))
                    return;

                controller.IsAggressive = true;
            }
        }

        public void MakePassive()
        {
            foreach (Selectable s in SelectedObjects.Selected)
            {
                if (!s.gameObject.TryGetComponent(out StateControllerUnit controller))
                    return;

                controller.IsAggressive = false;
            }
        }
    }
}