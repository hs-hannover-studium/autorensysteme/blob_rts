﻿using Kniblings.Controllers;
using Kniblings.Utility;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Kniblings.UI.Menu
{
    public class SettingsMenu : MonoBehaviour
    {
        [FoldoutGroup("Audi")]
        [SerializeField]
        private Slider fxSlider;

        [FoldoutGroup("Audi")]
        [SerializeField]
        private Slider masterSlider;

        [FoldoutGroup("Audi")]
        [SerializeField]
        private Slider musicSlider;

        private void OnEnable()
        {
            masterSlider.value = Mathf.Pow(10, AudioController.Instance.masterVolume.RuntimeValue / 20);
            fxSlider.value = Mathf.Pow(10, AudioController.Instance.fxVolume.RuntimeValue / 20);
            musicSlider.value = Mathf.Pow(10, AudioController.Instance.musicVolume.RuntimeValue / 20);
        }

        public void SetMasterVolume(float value)
        {
            AudioController.Instance.SetVolume(Mathf.Log10(value) * 20, AudioChannel.Master);
        }

        public void SetMusicVolume(float value)
        {
            AudioController.Instance.SetVolume(Mathf.Log10(value) * 20, AudioChannel.Music);
        }

        public void SetFxVolume(float value)
        {
            AudioController.Instance.SetVolume(Mathf.Log10(value) * 20, AudioChannel.Fx);
        }

        public void SetScreenResolution(int i)
        {
        }

        public void SetFullscreen(bool isFullscreen)
        {
        }
    }
}