﻿using Kniblings.Controllers;
using UnityEngine;

namespace Kniblings.UI.Menu
{
    public class PauseMenu : MonoBehaviour
    {
        public GameObject gameUi;
        public GameObject pauseMenu;
        public GameObject[] windows;

        private void OnEnable()
        {
            GameController.Instance.GamePaused += OpenMenu;
            GameController.Instance.GameResumed += CloseMenu;
        }

        private void OnDisable()
        {
            GameController.Instance.GamePaused -= OpenMenu;
            GameController.Instance.GameResumed -= CloseMenu;
        }

        public void CloseAllWindows()
        {
            foreach (GameObject o in windows) o.SetActive(false);
        }

        public void OpenMenu()
        {
            pauseMenu.SetActive(true);
            gameUi.SetActive(false);
        }

        public void CloseMenu()
        {
            gameUi.SetActive(true);
            pauseMenu.SetActive(false);

            CloseAllWindows();
        }
    }
}