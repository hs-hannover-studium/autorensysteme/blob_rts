﻿using Kniblings.Controllers;
using UnityEditor;
using UnityEngine;

namespace Kniblings.UI.Menu
{
    public class MainMenu : MonoBehaviour
    {
        public void LoadScene(int index)
        {
            SceneController.LoadScene(index);
        }

        public void OnSettings()
        {
            //TODO
        }

        public void OnQuit()
        {
            Application.Quit();

#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#endif
        }
    }
}