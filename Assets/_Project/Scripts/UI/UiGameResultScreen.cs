﻿using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.World;
using Kniblings.World.Buildings;
using Kniblings.World.BuildMode;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiGameResultScreen : MonoBehaviour
    {
        [Required] public GameObject gameUi;
        [Required] public GameObject resultScreen;
        [Required] public GameObject gameLostTitle;
        [Required] public GameObject gameWonTitle;
        [Required] public PlayerStatsWindow playerStatsWindow;

        [Title("Player Stats")] [SerializeField, ReadOnly]
        private int kniblingsCreated;

        [SerializeField, ReadOnly] private int enemiesKilled;
        [SerializeField, ReadOnly] private int buildingsBuild;
        [SerializeField, ReadOnly] private ResourcePackage resourcesGathered;

        public void OnEnable()
        {
            GameController.Instance.GameWon += OnGameWon;
            GameController.Instance.GameLost += OnGameLost;

            VillageCore.KniblingCreated += OnKniblingCreated;
            WaveController.Instance.EnemyKilled += OnEnemyKilled;
            BuildModePlacement.BuildingBuild += OnBuildingBuild;
            ResourceSpawner.ResourceCollected += OnResourceCollected;
            PlayerController.Instance.ResourceGenerated += InstanceOnResourceGenerated;
        }

        private void InstanceOnResourceGenerated(ResourcePackage obj)
        {
            resourcesGathered += obj;
        }

        private void OnResourceCollected(ResourceCollectible obj)
        {
            resourcesGathered += obj.Resource.ToResourcePackage();
        }

        private void OnDisable()
        {
            GameController.Instance.GameWon -= OnGameWon;
            GameController.Instance.GameLost -= OnGameLost;
        }

        private void OnGameLost()
        {
            FillStatsTable();
            resultScreen.SetActive(true);
            gameWonTitle.SetActive(false);
            gameLostTitle.SetActive(true);
            gameUi.SetActive(false);
        }

        private void OnGameWon()
        {
            FillStatsTable();
            resultScreen.SetActive(true);
            gameWonTitle.SetActive(true);
            gameLostTitle.SetActive(false);
            gameUi.SetActive(false);
        }

        private void FillStatsTable()
        {
            playerStatsWindow.SetTextValues(kniblingsCreated, enemiesKilled, buildingsBuild, resourcesGathered.malisium,
                resourcesGathered.stone, resourcesGathered.wood);
        }

        private void OnBuildingBuild()
        {
            buildingsBuild++;
        }

        private void OnEnemyKilled()
        {
            enemiesKilled++;
        }

        private void OnKniblingCreated()
        {
            kniblingsCreated++;
        }
    }
}