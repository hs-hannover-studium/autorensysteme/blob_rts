﻿using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiUnitIconMore : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI countText;

        public void Init(int count)
        {
            countText.SetText("+" + count);
        }
    }
}