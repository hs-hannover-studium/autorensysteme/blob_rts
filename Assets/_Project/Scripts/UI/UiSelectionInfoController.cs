﻿using System;
using Kniblings.Data;
using Kniblings.Data.Enums;
using Kniblings.Selection;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiSelectionInfoController : MonoBehaviour
    {
        private GameObject _currentInfoUi;
        [BoxGroup] public GameObject buildingInfo;
        [BoxGroup] public GameObject coreInfo;
        [BoxGroup] public GameObject multiUnitInfo;
        [BoxGroup] public GameObject unitInfo;

        private void OnEnable()
        {
            SelectedObjects.SelectionChanged += SelectedObjectsOnSelectionChanged;
        }

        private void OnDisable()
        {
            SelectedObjects.SelectionChanged -= SelectedObjectsOnSelectionChanged;
        }

        private void SelectedObjectsOnSelectionChanged(int count)
        {
            try
            {
                _currentInfoUi.SetActive(false);
            }
            catch (Exception e)
            {
                //ignored
            }

            if (count == 0)
                return;

            switch (SelectedObjects.SelectionType)
            {
                case SelectionType.Moveable:
                    _currentInfoUi = count == 1 ? unitInfo : multiUnitInfo;
                    _currentInfoUi.SetActive(true);
                    break;
                case SelectionType.Single:
                    _currentInfoUi =
                        ((SelectableBuilding) SelectedObjects.Selected[0]).BuildingBase.data.type ==
                        BuildingType.VillageCore
                            ? coreInfo
                            : buildingInfo;
                    _currentInfoUi.SetActive(true);
                    break;
                case SelectionType.None:
                    _currentInfoUi.SetActive(false);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}