﻿using Kniblings.Data;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiResourceBar : MonoBehaviour
    {
        public UiResourceElement malisiumCounter;
        public UiResourceElement pebbleCounter;

        public RuntimeResourceData resourceData;
        public UiResourceElement stickCounter;

        private void OnEnable()
        {
            resourceData.RuntimeValuesChanged += OnRuntimeValuesChanged;
        }

        private void OnDisable()
        {
            resourceData.RuntimeValuesChanged -= OnRuntimeValuesChanged;
        }

        private void OnRuntimeValuesChanged(ResourcePackage resourcePackage)
        {
            malisiumCounter.UpdateCount(resourcePackage.malisium);
            stickCounter.UpdateCount(resourcePackage.wood);
            pebbleCounter.UpdateCount(resourcePackage.stone);
        }
    }
}