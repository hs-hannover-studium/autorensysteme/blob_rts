﻿using System;
using System.Collections.Generic;
using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Locomotion;
using Kniblings.Other;
using Kniblings.Selection;
using Kniblings.StateMachine;
using Kniblings.StateMachine.Unit;
using Kniblings.World.Buildings;
using UnityEngine;

namespace Kniblings.UI
{
    [Serializable]
    public class UiJobAssignment : MonoBehaviour
    {
        private List<StateControllerUnit> _units;
        public JobType jobType;
        
        public void Init(List<StateControllerUnit> units)
        {
            _units = units;
        }

        public void AssignJob()
        {
            foreach (StateControllerUnit unit in _units)
            {
                unit.BaseObject.ChangeJob(jobType);
            }
        }
    }
}