using System.Collections.Generic;
using Kniblings.Selection;
using Kniblings.StateMachine.Unit;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiSelectionInfoMultiUnit : MonoBehaviour
    {
        private Selectable[] _units;
        public UiUnitGrid grid;

        private void OnEnable()
        {
            _units = SelectedObjects.Selected.ToArray();
            grid.Init(_units);


            List<StateControllerUnit> unitList = MakeStateControllerUnits();

            foreach (UiJobAssignment component in GetComponentsInChildren<UiJobAssignment>()) component.Init(unitList);
        }

        private void OnDisable()
        {
            grid.Clear();
        }

        private List<StateControllerUnit> MakeStateControllerUnits()
        {
            List<StateControllerUnit> list = new List<StateControllerUnit>();
            foreach (SelectableUnit unit in _units) list.Add(unit.StateController);

            return list;
        }
    }
}