﻿using Kniblings.Controllers;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiWaves : MonoBehaviour
    {
        public TextMeshProUGUI text;
        public GameObject waveUI;

        private void OnEnable()
        {
            WaveController.Instance.WaveStarted += OnWaveStarted;
        }

        private void OnDisable()
        {
            WaveController.Instance.WaveStarted += OnWaveStarted;
        }

        private void OnWaveStarted()
        {
            text.SetText("Wave " + (WaveController.Instance.CurrentWaveIndex));
            waveUI.SetActive(true);
        }
    }
}