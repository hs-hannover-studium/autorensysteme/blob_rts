﻿using UnityEngine;

namespace Kniblings.UI
{
    public class UiBillboard : MonoBehaviour
    {
        private Transform _cameraTransform;

        private void Start()
        {
            //TODO unhack this
            _cameraTransform = Camera.main.transform;
        }

        private void LateUpdate()
        {
            transform.LookAt(_cameraTransform);
        }
    }
}