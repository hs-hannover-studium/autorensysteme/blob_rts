﻿using DG.Tweening;
using Kniblings.Controllers;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiButtonEffects : MonoBehaviour
    {
        private RectTransform _textTransform;
        public Ease easeTypeClick;
        public Ease easeTypeHover;
        public float scaleToSizeClick = .95f;
        public float scaleToSizeHover = 1.05f;
        public TextMeshProUGUI text;

        private void Start()
        {
            _textTransform = text.GetComponent<RectTransform>();
        }

        public void OnClick()
        {
            AudioController.Instance.PlayFxOneShot(AudioController.Instance.uiBtnClick);

            _textTransform.DOScale(scaleToSizeClick, .1f).SetEase(easeTypeClick).OnComplete(() =>
            {
                _textTransform.DOScale(1, .1f).SetEase(easeTypeClick);
            });
        }

        public void OnHover()
        {
            AudioController.Instance.PlayFxOneShot(AudioController.Instance.uiBtnHover);

            _textTransform.DOScale(scaleToSizeHover, .25f).SetEase(easeTypeHover);
        }

        public void OnExit()
        {
            _textTransform.DOScale(1, .25f).SetEase(easeTypeHover);
        }
    }
}