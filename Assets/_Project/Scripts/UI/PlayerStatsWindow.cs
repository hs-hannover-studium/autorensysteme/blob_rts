﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class PlayerStatsWindow : MonoBehaviour
    {
        [SerializeField, Required] private TextMeshProUGUI kniblingsCreatedValueText;
        [SerializeField, Required] private TextMeshProUGUI enemiesKilledValueText;
        [SerializeField, Required] private TextMeshProUGUI buildingsBuildValueText;
        [SerializeField, Required] private TextMeshProUGUI malisiumGatheredValueText;
        [SerializeField, Required] private TextMeshProUGUI stoneGatheredValueText;
        [SerializeField, Required] private TextMeshProUGUI woodGatheredValueText;
        [SerializeField, Required] private TextMeshProUGUI playTimeValueText;

        public void SetTextValues(int kniblingsCreated, int enemiesKilled, int buildingsBuild, int malisiumGathered,
            int stoneGathered, int woodGathered)
        {
            kniblingsCreatedValueText.SetText(kniblingsCreated.ToString());
            enemiesKilledValueText.SetText(enemiesKilled.ToString());
            buildingsBuildValueText.SetText(buildingsBuild.ToString());
            malisiumGatheredValueText.SetText(malisiumGathered.ToString());
            stoneGatheredValueText.SetText(stoneGathered.ToString());
            woodGatheredValueText.SetText(woodGathered.ToString());

            int playTimeInSec = (int) Time.timeSinceLevelLoad;
            playTimeValueText.SetText(playTimeInSec + " Sec");
        }
    }
}