﻿using System;
using Kniblings.Controllers;
using Kniblings.Data;
using Kniblings.Selection;
using Kniblings.World.Buildings;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiSelectionInfoBuilding : MonoBehaviour
    {
        private SelectableBuilding _selectable;
        public TextMeshProUGUI descriptionText;
        public TextMeshProUGUI nameText;
        public GameObject capacityWrapper;
        public TextMeshProUGUI capacityText;

        private void OnEnable()
        {
            _selectable = (SelectableBuilding) SelectedObjects.Selected[0];

            nameText.SetText(_selectable.BuildingBase.data.Name);
            descriptionText.SetText(_selectable.BuildingBase.data.Description);

            try
            {
                PlaceableBuilding placeableBuilding = (PlaceableBuilding) _selectable.BuildingBase;

                if (placeableBuilding.SpecialData.capacity > 0)
                {
                    capacityWrapper.SetActive(true);
                    capacityText.SetText(placeableBuilding.AssignedUnits.Count + "/" +
                                         placeableBuilding.SpecialData.capacity);
                }
            }
            catch (Exception e)
            {
                // ignore
            }
        }

        private void OnDisable()
        {
            _selectable = null;
            nameText.SetText("");
            descriptionText.SetText("");
            capacityText.SetText("");

            capacityWrapper.SetActive(false);
        }

        // Called by Unity Button
        public void OnDemolishButton()
        {
            _selectable.BuildingBase.Demolish();
            PlayerController.Instance.AddResources(_selectable.BuildingBase.data.ResourceCost);
            SelectedObjects.DeselectAll();
        }
    }
}