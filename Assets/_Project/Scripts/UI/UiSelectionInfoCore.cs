﻿using System;
using Kniblings.Data;
using Kniblings.Selection;
using Kniblings.World.Buildings;
using TMPro;
using UnityEngine;

namespace Kniblings.UI
{
    public class UiSelectionInfoCore : MonoBehaviour
    {
        private SelectableBuilding _selectable;
        public TextMeshProUGUI descriptionText;
        public TextMeshProUGUI nameText;
        
        [SerializeField] private TextMeshProUGUI malisiumTxt;
        [SerializeField] private TextMeshProUGUI woodTxt;
        [SerializeField] private TextMeshProUGUI stoneTxt;

        private void OnEnable()
        {
            _selectable = (SelectableBuilding) SelectedObjects.Selected[0];

            VillageCore vc = (VillageCore) _selectable.BuildingBase;
            UnitData unitData = vc.unitData;

            nameText.SetText(_selectable.BuildingBase.data.Name);
            descriptionText.SetText(_selectable.BuildingBase.data.Description);
            
            malisiumTxt.SetText(unitData.ResourceCost.malisium > 0 ? unitData.ResourceCost.malisium.ToString() : "0");
            woodTxt.SetText(unitData.ResourceCost.wood > 0 ? unitData.ResourceCost.wood.ToString() : "0");
            stoneTxt.SetText(unitData.ResourceCost.stone > 0 ? unitData.ResourceCost.stone.ToString() : "0");
        }

        public void OnMakeUnitButton()
        {
            VillageCore vc = (VillageCore) _selectable.BuildingBase;
            vc.SpawnVillager();
        }
    }
}