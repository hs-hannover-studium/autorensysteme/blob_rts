﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Kniblings.Editor
{
    public static class GameObjectExtensions
    {
        public static Texture2D GetPrefabPreview(this GameObject prefab)
        {
            if (!PrefabUtility.IsPartOfAnyPrefab(prefab))
                throw new ArgumentException($"{prefab.name} is not part of a prefab");
            
            string assetPath = AssetDatabase.GetAssetPath(prefab);
            Object asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
            return AssetPreview.GetAssetPreview(asset);
        }
    }
}