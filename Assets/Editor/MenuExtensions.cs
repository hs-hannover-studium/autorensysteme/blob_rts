﻿using UnityEditor;
using UnityEngine;

namespace Kniblings.Editor
{
    public static class MenuExtensions
    {

        [MenuItem("Custom/Create/Building Prefab")]
        public static void CreateBuildingPrefab()
        {
            string path = "Assets/_Project/Prefabs/Buildings/";
            GameObject obj =
                AssetDatabase.LoadAssetAtPath<GameObject>(path + "__Building_Base.prefab");
            GameObject instance = (GameObject) PrefabUtility.InstantiatePrefab(obj);
            PrefabUtility.SaveAsPrefabAsset(instance, path + "newBuilding.prefab");
            Object.DestroyImmediate(instance);
        }
        
        [MenuItem("GameObject/Create Zero Empty", priority = 0)]
        public static void CreateEmpty()
        {
            GameObject empty = new GameObject("GameObject");
            Undo.RegisterCreatedObjectUndo(empty, "Create New Empty");

            if (UnityEditor.Selection.activeTransform != null)
            {
                empty.transform.SetParent(UnityEditor.Selection.activeTransform, false);
            }

            // empty.AddComponent<MyComponent>();
	
            UnityEditor.Selection.objects = new Object[] {empty};
        }

        [MenuItem("GameObject/Zero Out GameObject")]
        public static void ZeroOutGameObject()
        {
            Transform[] selected = UnityEditor.Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selected, "Zero Out GameObjects");
            foreach (Transform t in selected)
            {
                t.position = Vector3.zero;
                t.localRotation = Quaternion.identity;
                t.localScale = Vector3.zero;
            }
        }
    }
}
